## frozenpeek

 - uses gunicorn as application server to run the django application behind a nginx reverse proxy
 - uses [huey](https://github.com/coleifer/huey) as task queue to schedule or run periodic [tasks](https://git.y0fl0w.de/y0fl0w/mouz/src/branch/master/mouz/home/tasks.py)
 - uses [discord.py](https://github.com/Rapptz/discord.py) to implement [discord bot functionallity](https://git.y0fl0w.de/y0fl0w/mouz/src/branch/master/mouz/ropzpeek/management/commands)
 - uses redis to sync between the sync django and the async discord py world