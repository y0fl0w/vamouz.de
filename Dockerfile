FROM python:3.9.13

ENV VIRTUAL_ENV=/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

ADD reqs.req /app/reqs.req

RUN  python -m venv /venv 
RUN /venv/bin/pip install --upgrade pip 
RUN /venv/bin/pip install --no-cache-dir -r /app/reqs.req 

ADD mouz/ /app
ADD .env /app/.env
ADD run_web.sh /app/run_web.sh
WORKDIR /app

CMD ["bash", "run_web.sh"]
