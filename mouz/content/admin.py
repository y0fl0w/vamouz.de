from . import models
from django.contrib import admin
from django.forms import ModelForm
from tagulous.admin import register


class ContentAdmin(admin.ModelAdmin):
    search_fields = ["name", "user__username"]


admin.site.register(models.Content, ContentAdmin)
register(models.Content.tags)
