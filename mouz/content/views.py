from . import forms, models
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key
from django.dispatch import receiver
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import CreateView
from home.tasks import send_content_msg


class ContentCreateView(LoginRequiredMixin, CreateView):
    model = models.Content
    form_class = forms.ContentForm

    def get_success_url(self):
        return reverse("home:home", kwargs={})

    def form_valid(self, form):
        form.instance.user = self.request.user
        self.object = form.save()
        send_content_msg(self.object.pk)
        return HttpResponseRedirect(self.get_success_url())


class ContentListView(ListView):
    model = models.Content
    paginate_by = 12
    ordering = ["-date"]

    def get_queryset(self):
        queryset = models.Content.objects.all()
        tag = self.request.GET.get("tag")
        if tag:
            queryset = queryset.filter(tags__name__icontains=tag)
        return queryset.distinct().order_by("-date")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tags"] = models.Content.tags.tag_model.objects.all()
        return context
