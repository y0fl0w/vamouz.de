from . import models
from django.forms import ModelForm


class ContentForm(ModelForm):
    class Meta:
        model = models.Content
        fields = ["name", "url", "tags"]
        labels = {
            "name": "Title",
            "url": "Link",
        }
        help_texts = {
            "name": "Please provide a meaningful title",
            "url": "The URL the content is available at",
        }
