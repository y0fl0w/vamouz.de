# Generated by Django 3.1 on 2021-06-23 12:26

import tagulous.models.fields
import tagulous.models.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("content", "0003_auto_20210613_2243"),
    ]

    operations = [
        migrations.CreateModel(
            name="Tagulous_Content_tags",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=255, unique=True)),
                ("slug", models.SlugField()),
                (
                    "count",
                    models.IntegerField(
                        default=0,
                        help_text="Internal counter of how many times this tag is in use",
                    ),
                ),
                (
                    "protected",
                    models.BooleanField(
                        default=False,
                        help_text="Will not be deleted when the count reaches 0",
                    ),
                ),
            ],
            options={
                "ordering": ("name",),
                "abstract": False,
                "unique_together": {("slug",)},
            },
            bases=(tagulous.models.models.BaseTagModel, models.Model),
        ),
        migrations.AddField(
            model_name="content",
            name="tags",
            field=tagulous.models.fields.TagField(
                _set_tag_meta=True,
                help_text="Enter a comma-separated tag string",
                to="content.Tagulous_Content_tags",
            ),
        ),
    ]
