# Generated by Django 3.1 on 2021-08-26 19:35

import tagulous.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("content", "0004_auto_20210623_1426"),
    ]

    operations = [
        migrations.AlterField(
            model_name="content",
            name="tags",
            field=tagulous.models.fields.TagField(
                _set_tag_meta=True,
                blank=True,
                help_text="Enter a comma-separated tag string",
                to="content.Tagulous_Content_tags",
            ),
        ),
    ]
