from . import views
from django.urls import path

app_name = "content"
urlpatterns = [
    path("add/", views.ContentCreateView.as_view(), name="add"),
    path("", views.ContentListView.as_view(), name="list"),
]
