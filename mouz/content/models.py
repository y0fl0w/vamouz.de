from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from tagulous.models import TagField


class Content(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField()
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        get_user_model(), null=True, blank=True, on_delete=models.CASCADE
    )
    clicks = models.IntegerField(default=0)
    tags = TagField(blank=True)

    def __str__(self):
        return self.name
