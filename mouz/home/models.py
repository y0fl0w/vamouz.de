import cloudscraper
import json
import logging
import pytz
import re
import redis
import requests
import ssl
from bs4 import BeautifulSoup
from datetime import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from home.tasks import send_match_msg
from huey.contrib.djhuey import HUEY
from markdownfield.models import MarkdownField, RenderedMarkdownField
from markdownfield.validators import VALIDATOR_NULL, VALIDATOR_STANDARD

rs = redis.Redis(host="redis", port=6379, db=0)
logger = logging.getLogger(__name__)

PROXIES = {
    "http": "http://{}:{}@brd.superproxy.io:33335".format(
        settings.BRIGHTDATA_USER, settings.BRIGHTDATA_PW
    ),
    "https": "http://{}:{}@brd.superproxy.io:33335".format(
        settings.BRIGHTDATA_USER, settings.BRIGHTDATA_PW
    ),
}


class Team(models.Model):
    name = models.CharField(max_length=255)
    no_match_msg = models.CharField(max_length=255, blank=True, null=True)
    hltv_id = models.IntegerField(blank=True, null=True)
    vlr_id = models.IntegerField(blank=True, null=True)
    pl_id = models.IntegerField(blank=True, null=True)
    rank = models.IntegerField(default=99)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Site(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(
        max_length=255,
        unique=True,
        help_text="The slug the site is available through: https://vamouz.de/slug",
    )
    content = MarkdownField(
        rendered_field="content_rendered",
        validator=VALIDATOR_NULL,
        blank=True,
        help_text="Site text",
    )
    content_rendered = RenderedMarkdownField()

    def __str__(self):
        return self.name


class BaseEvent(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True, blank=True, null=True)
    desc = MarkdownField(
        rendered_field="desc_rendered",
        validator=VALIDATOR_NULL,
        blank=True,
        help_text="Event description",
    )
    desc_rendered = RenderedMarkdownField()
    starting_at = models.DateTimeField(null=True, blank=True, help_text="Start date")
    ending_at = models.DateTimeField(null=True, blank=True, help_text="End date")

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(BaseEvent, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def is_active(self):
        now = timezone.now()
        if now > self.starting_at and now < self.ending_at:
            return True
        return False

    def is_finished(self):
        now = timezone.now()
        if now > self.ending_at:
            return True


class Event(BaseEvent):
    data = models.JSONField(blank=True, null=True, default=dict)


class Match(models.Model):
    date = models.DateTimeField(null=True, blank=True)
    date_guessed = models.BooleanField(default=False)
    postponed = models.BooleanField(default=False)
    status = models.CharField(
        max_length=15,
        default="upcoming",
        choices=(
            ("upcoming", "Upcoming"),
            ("finished", "Finished"),
        ),
    )
    game = models.CharField(
        max_length=15,
        default="csgo",
        choices=(
            ("csgo", "CS:GO"),
            ("lol", "League of Legends"),
            ("valo", "Valorant"),
            ("sc2", "StarCraft II"),
            ("wc3", "Warcraft III"),
            ("fortnite", "Fortnite"),
            ("sf", "Street Fighter"),
            ("sim", "Sim Racing"),
            ("dota", "Dota 2"),
        ),
    )
    hltv_id = models.IntegerField(blank=True, null=True)
    esea_id = models.IntegerField(blank=True, null=True)
    vlr_id = models.IntegerField(blank=True, null=True)
    vlr_dach_id = models.IntegerField(blank=True, null=True)
    pl_id = models.IntegerField(blank=True, null=True)
    faceit_id = models.CharField(max_length=255, null=True, blank=True)
    event_url = models.URLField(blank=True, null=True)
    event_name = models.CharField(max_length=255, blank=True, null=True)
    stream = models.URLField(blank=True, null=True)
    hltv_data = models.JSONField(blank=True, null=True, default=dict)
    noti_counter = models.IntegerField(default=0)
    locked = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        orig = Match.objects.filter(pk=self.pk).first()
        super(Match, self).save(*args, **kwargs)
        self.reschedule()

    def __str__(self):
        team_a_name = ""
        team_a = self.teams.filter(slot="a").first()
        if team_a:
            team_a_name = team_a.team.name
        team_b_name = ""
        team_b = self.teams.filter(slot="b").first()
        if team_b:
            team_b_name = team_b.team.name
        return "{} vs. {}".format(team_a_name, team_b_name)

    def update_match(self):
        logger = logging.getLogger(__name__)
        if self.game == "csgo":
            if not self.hltv_id:
                return
            if self.locked:
                return
            scraper = cloudscraper.create_scraper()
            scraper.proxies.update(PROXIES)
            logger.warning("extra fetch {}".format(self))
            MATCH_URL = "http://hltv.org/mobile/MatchInfo?matchId={}".format(
                self.hltv_id
            )
            r = scraper.get(MATCH_URL)
            try:
                data = r.json()
            except:
                logger.warning(
                    "malformed reply for match fetch {}".format(self.hltv_id)
                )
                return
            self.hltv_data = data
            if data.get("match", {}).get("postMatch", False):
                self.status = "finished"
            else:
                self.status = "upcoming"
            starting_at = data.get("match", {}).get("startDateTime")
            schedule = not self.has_notifications()
            if starting_at:
                date = timezone.make_aware(
                    timezone.datetime.fromisoformat(starting_at[:-1]),
                    pytz.timezone("utc"),
                )
                if self.date != date:
                    schedule = True
                self.date = date
            self.save()
            if schedule:
                self.reschedule()
        elif self.game in ["dota", "lol"]:
            if not self.pl_id:
                return
            link = self.get_match_link().get("link")
            if not link:
                return
            logger.warning("extra fetch {}".format(self))
            scraper = cloudscraper.create_scraper()
            r = requests.get(link)
            soup = BeautifulSoup(r.content, "html.parser")
            date = soup.find("span", {"class": "tztime"})["data-time"]
            if not date:
                date = soup.find("span", {"class": "itime"})["data-time"]
            if date:
                date = datetime.utcfromtimestamp(int(date)).replace(tzinfo=pytz.utc)
                schedule = not self.has_notifications()
                if self.date != date:
                    schedule = True
                    self.date = date
                self.save()

            match_data = {}
            match_header = soup.find("div", {"class": "content-match-head"})
            result = match_header.find(
                "div", {"class": "content-match-head-status"}
            ).find_all("span")
            score_a, score_b = 0, 0
            if len(result) > 1:
                split = result[1].text.split(":")
                score_a, score_b = int(split[0]), int(split[1])
            team_a = (
                match_header.find("div", {"class": "content-match-head-team1"})
                .find("h2")
                .text
            )
            team_b = (
                match_header.find("div", {"class": "content-match-head-team2"})
                .find("h2")
                .text
            )
            if team_b in ["MOUZ", "NXT"]:
                team_a, team_b = team_b, team_a
                score_a, score_b = score_b, score_a
            event_box = (
                soup.find("ul", {"class": "breadcrumbs"}).find_all("li")[1].find("a")
            )
            event_url = event_box.attrs.get('href')
            event_name = event_box.span.get_text()
            match_data = {
                "score_a": score_a,
                "score_b": score_b,
                "team_a": team_a,
                "team_b": team_b,
                "event_url": event_url,
                "event_name": event_name,
            }
            print(match_data)
            self.update_data(match_data)
        elif self.game == "valo":
            if not self.vlr_dach_id:
                return
            logger.warning("extra fetch {}".format(self))
            scraper = cloudscraper.create_scraper()
            r = requests.get(
                "https://www.vrldachevolution.gg/matches/{}-vamouz".format(
                    self.vlr_dach_id
                )
            )
            soup = BeautifulSoup(r.content, "html.parser")
            date = soup.find("span", {"class": "tztime"})["data-time"]
            date = datetime.utcfromtimestamp(int(date)).replace(tzinfo=pytz.utc)
            schedule = not self.has_notifications()
            print(date)
            if self.date != date:
                schedule = True
                self.date = date
            self.save()
            if schedule:
                self.reschedule()

    def update_data(self, data):
        if not self.event_url:
            self.event_url = data.get("event_url")
        if not self.event_name:
            self.event_name = data.get("event_name")
        match_team_a = self.teams.filter(slot="a").first()
        match_team_b = self.teams.filter(slot="b").first()
        if not match_team_a:
            match_team_a = MatchTeam()
            team_a_id = data.get("team_a_id", "-1")
            team_a_id = int(team_a_id)
            team_a_name = data.get("team_a", "TBA")
            team_a = Team.objects.filter(name=team_a_name).first()
            if not team_a and team_a_id != -1:
                team_a = Team()
                team_a.name = team_a_name
                team_a.save()
            match_team_a.match = self
            match_team_a.team = team_a
            match_team_a.score = data.get("score_a", 0)
            match_team_a.slot = "a"
        if not match_team_b or match_team_b.team.name in ['TBD', 'TBA']:
            if not match_team_b:
                match_team_b = MatchTeam()
            team_b_id = data.get("team_b_id", "-1")
            team_b_id = int(team_b_id)
            team_b_name = data.get("team_b", "TBA")
            team_b = Team.objects.filter(name=team_b_name).first()
            if not team_b:
                team_b = Team()
                team_b.name = team_b_name
                team_b.save()
            elif not team_b:
                team_b = Team.objects.filter(name="TBD").first()
            match_team_b.match = self
            match_team_b.team = team_b
            match_team_b.score = data.get("score_b", 0)
            match_team_b.slot = "b"
        match_team_a.score = data.get("score_a", 0)
        match_team_b.score = data.get("score_b", 0)
        if match_team_a.score > 0 or match_team_b.score > 0:
            self.status = "finished"
        match_team_a.save()
        match_team_b.save()
        self.save()

    def get_data(self):
        data = {"status": self.status, "date": self.date}
        team_a = self.teams.filter(slot="a").first()
        if team_a:
            team_a_name = team_a.team.name
            data["name_a"] = team_a_name
            data["score_a"] = team_a.score
        else:
            data["name_a"] = "TBA"
        team_b = self.teams.filter(slot="b").first()
        if team_b:
            team_b_name = team_b.team.name
            data["name_b"] = team_b_name
            data["score_b"] = team_b.score
        else:
            data["name_b"] = "TBA"
        return data

    def has_notifications(self):
        for t in HUEY.scheduled():
            if t.kwargs.get("match_id", -1) == self.id:
                if not HUEY.is_revoked(t):
                    return True
        return False

    def schedule_notifications(self):
        logger.warning("schedule")
        times = settings.MATCH_ALERT_TIMES
        if not self.date or self.noti_counter >= len(times):
            return False
        if self.noti_counter > 0:
            times = times[self.noti_counter :]
        for t in times:
            if timezone.now() + timezone.timedelta(minutes=t) > self.date:
                if self.noti_counter < 2 and t == 1:
                    logger.warning("send instant notification for {}".format(self))
                    send_match_msg.schedule(kwargs={"match_id": self.pk}, delay=t)
                continue
            eta = self.date - timezone.timedelta(minutes=t)
            send_match_msg.schedule(kwargs={"match_id": self.pk}, eta=eta)
        return True

    def unschedule_notifications(self):
        ret_val = False
        for t in HUEY.scheduled():
            if t.kwargs.get("match_id", -1) == self.id:
                ret_val = True
                HUEY.revoke(t)

    def reschedule(self):
        logger.warning("reschedule {}".format(self))
        self.unschedule_notifications()
        self.schedule_notifications()

    def get_upcoming_content(self, include_team=False):
        mouz = ""
        timestamp = int(self.date.timestamp())
        date_string = "**<t:{}:R>** | <t:{}>".format(timestamp, timestamp)
        if self.postponed:
            date_string = "_POSTPONED_"
        if self.date_guessed:
            date_string = date_string + " | _estimated_"
        if (
            self.status == "upcoming"
            and self.date < timezone.now()
            and not self.postponed
        ):
            date_string = "_L I V E_"
        if self.game == "lol":
            opponent = str(self).split(" vs. ")[1]
            if self.pl_id:
                mouz = "**[vs. {}]({})** at [{}]({})\n{}".format(
                    opponent,
                    "https://www.primeleague.gg/matches/{}-vamouz".format(self.pl_id),
                    self.get_event_name(),
                    self.event_url,
                    date_string,
                )
            else:
                mouz = "**vs. {}** at [{}]({})\n{}".format(
                    opponent, self.get_event_name(), self.event_url, date_string
                )
        if self.game == "dota":
            opponent = str(self).split(" vs. ")[1]
            if self.pl_id:
                mouz = "**[vs. {}]({})** at [{}]({})\n{}".format(
                    opponent,
                    "https://www.joindota.com/matches/{}-vamouz".format(self.pl_id),
                    self.get_event_name(),
                    self.event_url,
                    date_string,
                )
            else:
                mouz = "**vs. {}** at [{}]({})\n{}".format(
                    opponent, self.get_event_name(), self.event_url, date_string
                )
        elif self.game == "valo":
            opponent = str(self).split(" vs. ")[1]
            if self.vlr_id or self.vlr_dach_id:
                opponent = str(self).split(" vs. ")[1]
                if self.vlr_id:
                    match_url = "https://vlr.gg/{}/vamouz".format(self.vlr_id)
                elif self.vlr_dach_id > 0:
                    match_url = (
                        "https://www.vrldachevolution.gg/matches/{}-vamouz".format(
                            self.vlr_dach_id
                        )
                    )
                mouz = "**[vs. {}]({})** at [{}]({})\n{}".format(
                    opponent,
                    match_url,
                    self.get_event_name(),
                    self.event_url,
                    date_string,
                )
            else:
                opponent = "vs. " + str(self).split(" vs. ")[1]
                mouz = "**{}** at [{}]({})\n{}".format(
                    opponent, self.get_event_name(), self.get_event_url(), date_string
                )

        elif self.game == "csgo":
            if self.hltv_id or self.esea_id or self.faceit_id:
                match_link = None
                if self.hltv_id:
                    match_link = "https://hltv.org/matches/{}/vamouz".format(
                        self.hltv_id
                    )
                elif self.esea_id:
                    match_link = "https://play.esea.net/match/{}".format(self.esea_id)
                elif self.faceit_id:
                    match_link = "https://www.faceit.com/en/csgo/room/{}".format(
                        self.faceit_id
                    )

                if not include_team:
                    opponent = "vs. " + str(self).split(" vs. ")[1]
                else:
                    opponent = str(self)
                if (
                    (self.hltv_id and self.hltv_id > 0)
                    or self.esea_id
                    or self.faceit_id
                ):
                    mouz = "**[{}]({})** at [{}]({})\n{}".format(
                        opponent,
                        match_link,
                        self.get_event_name(),
                        self.get_event_url(),
                        date_string,
                    )
                else:
                    mouz = "**vs. TBA** at [{}]({})\n{}".format(
                        self.get_event_name(), self.event_url, date_string
                    )
            else:
                if not include_team:
                    opponent = "vs. " + str(self).split(" vs. ")[1]
                else:
                    opponent = str(self)
                mouz = "**{}** at [{}]({})\n{}".format(
                    opponent, self.get_event_name(), self.get_event_url(), date_string
                )
        return mouz

    def get_result_content(self, include_team=False):
        mouz = ""
        timestamp = int(self.date.timestamp())
        date_string = "**<t:{}:R>**".format(timestamp, timestamp)
        score_a = self.teams.filter(slot="a").first().score
        score_b = self.teams.filter(slot="b").first().score
        icon = ":green_square:"
        if score_b > score_a:
            icon = ":red_square:"
        elif score_b == score_a:
            icon = ":orange_square:"
        opponent = str(self).split(" vs. ")[1]
        if self.game in ["lol", "dota"]:
            mouz = "{} **{}** [vs. {}]({}) {}".format(
                icon,
                "{0: <{width}}".format("{}:{}".format(score_a, score_b), width=5),
                opponent,
                self.event_url,
                date_string,
                # self.get_event_name(),
                #'https://hltv.org/events/{}/vamouz'.format(self.hltv_data.get('match', {}).get('eventId')),
            )
        elif self.game == "csgo":
            if not include_team:
                opponent = "vs. " + str(self).split(" vs. ")[1]
            else:
                opponent = str(self)
            match_link = None
            if self.hltv_id:
                match_link = "https://hltv.org/matches/{}/vamouz".format(self.hltv_id)
            elif self.esea_id:
                match_link = "https://play.esea.net/match/{}".format(self.esea_id)
            elif self.faceit_id:
                match_link = "https://www.faceit.com/en/csgo/room/{}".format(
                    self.faceit_id
                )
            mouz = "{} **{}** [{}]({}) {}".format(
                icon,
                "{0: <{width}}".format("{}:{}".format(score_a, score_b), width=5),
                opponent,
                match_link,
                date_string,
                # self.get_event_name(),
                #'https://hltv.org/events/{}/vamouz'.format(self.hltv_data.get('match', {}).get('eventId')),
            )
        elif self.game == "valo":
            mouz = "{} **{}** [vs. {}]({}) {}".format(
                icon,
                "{0: <{width}}".format("{}:{}".format(score_a, score_b), width=5),
                opponent,
                "https://www.vlr.gg/{}/vamouz".format(self.vlr_id),
                date_string,
                # self.get_event_name(),
                #'https://hltv.org/events/{}/vamouz'.format(self.hltv_data.get('match', {}).get('eventId')),
            )
        return mouz

    def get_event_name(self):
        val = None
        if self.event_name:
            val = self.event_name
        elif self.hltv_data:
            val = self.hltv_data.get("match", {}).get("eventName", "")
        if val:
            for s, d in [
                ("WePlay Academy League", "WAL"),
                ("Valorant Regional League", "VLR"),
            ]:
                val = val.replace(s, d)
            return val
        return "unknown"

    def get_event_url(self):
        if self.event_url:
            return self.event_url
        if self.hltv_data:
            return "https://hltv.org/events/{}/vamouz".format(
                self.hltv_data.get("match", {}).get("eventId")
            )
        return ""

    def get_match_link(self):
        if self.hltv_id and self.hltv_id > 0:
            return {
                "title": "HLTV",
                "link": "https://hltv.org/matches/{}/vamouz".format(self.hltv_id),
            }
        elif self.esea_id and self.esea_id > 0:
            return {
                "title": "ESEA",
                "link": "https://play.esea.net/match/{}".format(self.esea_id),
            }
        elif self.game == "lol" and self.pl_id and self.pl_id > 0:
            return {
                "title": "PL.gg",
                "link": "https://www.primeleague.gg/matches/{}-vamouz".format(
                    self.pl_id
                ),
            }
        elif self.game == "dota" and self.pl_id and self.pl_id > 0:
            return {
                "title": "JD.com",
                "link": "https://www.joindota.com/matches/{}-vamouz".format(self.pl_id),
            }
        elif self.vlr_id and self.vlr_id > 0:
            return {
                "title": "VLR.gg",
                "link": "https://www.vlr.gg/{}/vamouz".format(self.vlr_id),
            }
        elif self.vlr_dach_id and self.vlr_dach_id > 0:
            return {
                "title": "VLR DACH",
                "link": "https://www.vrldachevolution.gg/matches/{}-vamouz".format(
                    self.vlr_dach_id
                ),
            }
        elif self.faceit_id and self.faceit_id != "":
            return {
                "title": "FACEIT",
                "link": "https://www.faceit.com/en/csgo/room/{}".format(self.faceit_id),
            }
        return {"title": None, "link": None}


class ExtraMatch(models.Model):
    title = models.CharField(
        max_length=50, help_text="The title which is shown in the !upcoming command"
    )
    game = models.CharField(
        max_length=15,
        default="csgo",
        choices=(
            ("csgo", "CS:GO"),
            ("lol", "League of Legends"),
            ("valo", "Valorant"),
            ("sc2", "StarCraft II"),
            ("wc3", "Warcraft III"),
            ("fortnite", "Fortnite"),
            ("sf", "Street Fighter"),
            ("sim", "Sim Racing"),
        ),
    )
    starting_at = models.DateTimeField(
        null=True,
        blank=True,
        help_text="Date - if provided it will be used to render discord timestamps below the message",
    )
    date_guessed = models.BooleanField(default=False)
    message = models.TextField(
        help_text="The message below the title in the !upcoming command"
    )
    enabled = models.BooleanField(
        default=False, help_text="Shall this be included in the !upcoming reply?"
    )
    ending_at = models.DateTimeField(
        null=True,
        blank=True,
        help_text="Date - if provided it will be used to exclude the event after that date - even if enabled is set",
    )
    event_url = models.URLField(blank=True, null=True)
    event_name = models.CharField(max_length=255, blank=True, null=True)
    stream = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.title

    def get_upcoming_content(self, **kwargs):
        msg = self.message
        if self.starting_at:
            ts = int(self.starting_at.timestamp())
            extra = "\n**<t:{}:R>** | <t:{}:f>".format(ts, ts)
            if self.date_guessed:
                extra = extra + " | _estimated_"
            if self.starting_at < timezone.now():
                extra = "\n**L I V E**"
            msg = msg + extra
        return msg

    def get_event_name(self):
        val = None
        if self.event_name:
            val = self.event_name
        pattern = re.compile(r"\[(?P<event_name>.*)\]")
        match = pattern.search(self.message)
        if match and not val:
            match_dict = match.groupdict()
            if match_dict.get("event_name"):
                val = match_dict.get("event_name")
        if val:
            for s, d in [("WePlay Academy League", "WAL")]:
                val = val.replace(s, d)
            return val
        return "unknown"

    def get_event_url(self):
        if self.event_url:
            return self.event_url
        pattern = re.compile(r"\((?P<event_url>.*)\)")
        match = pattern.search(self.message)
        if match:
            match_dict = match.groupdict()
            if match_dict.get("event_url"):
                return match_dict.get("event_url")
        return ""


class MatchTeam(models.Model):
    match = models.ForeignKey(
        Match, null=True, related_name="teams", blank=True, on_delete=models.CASCADE
    )
    team = models.ForeignKey(Team, null=True, blank=True, on_delete=models.CASCADE)
    slot = models.CharField(
        max_length=15,
        default="a",
        choices=(
            ("a", "A"),
            ("b", "B"),
        ),
    )
    score = models.IntegerField(default=0)

    def __str__(self):
        return self.team.name


class Rank(models.Model):
    rank = models.IntegerField(default=1)
    ranking = models.CharField(max_length=255)


class Redirect(models.Model):
    slug = models.SlugField(max_length=255, unique=True)
    url = models.URLField()
    description = models.CharField(max_length=255, blank=True, null=True)
    img = models.ImageField(blank=True, null=True)
    customized = models.BooleanField(
        default=False,
        help_text="Whether it should be a straight redirect or a customized with changed social descriptions",
    )


class VerifiedUser(models.Model):
    user = models.ForeignKey(get_user_model(), blank=True, on_delete=models.CASCADE)
    discord_level = models.IntegerField(default=0)
    verified = models.BooleanField(default=False)
