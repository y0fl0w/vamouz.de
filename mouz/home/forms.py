from django import forms

CHANNEL_CHOICES = (
    ("441534100558446598", "#community-talk"),
    ("497900798638817280", "#match-talk"),
    ("489874067470680069", "#memes-and-banter"),
    ("494476435595984904", "#media-and-links"),
    ("484332981192753153", "#bot-commands"),
)


class MessageForm(forms.Form):
    channel = forms.ChoiceField(choices=CHANNEL_CHOICES)
    message = forms.CharField(widget=forms.Textarea)
