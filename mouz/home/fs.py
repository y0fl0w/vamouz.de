import json
import logging
import re
import requests

logger = logging.getLogger(__name__)

url = "http://localhost:8191/v1"
headers = {"Content-Type": "application/json"}


def fetch(url):
    data = {"cmd": "request.get", "url": url, "maxTimeout": 60000}
    response = requests.post(url, headers=headers, json=data)

    r = response.text

    logger.warning(response.status_code)
    logger.warning(response.text)
    rj = json.loads(response.text)
    logger.warning(rj)
    data = rj.get("solution").get("response")
    data_stripped = re.sub("<[^<]+?>", "", data)
    data_json = json.loads(data_stripped)
    return data_json
