import cloudscraper
import copy
import datetime
import json
import logging
import pytz
import redis
import requests
from discord.utils import escape_markdown
from django.conf import settings
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.utils import timezone

logger = logging.getLogger(__name__)
rs = redis.Redis(host="redis", port=6379, db=0)

DISCORD_WEBHOOK_MEDIA_LINKS = settings.DISCORD_WEBHOOK_MEDIA_LINKS
DISCORD_WEBHOOK_MATCH_ALERT = settings.DISCORD_WEBHOOK_MATCH_ALERT
DISCORD_WEBHOOK_STREAM_ALERT = settings.DISCORD_WEBHOOK_STREAM_ALERT
DISCORD_WEBHOOK_FACEIT_ALERT = settings.DISCORD_WEBHOOK_FACEIT_ALERT

MOUZ_FACEIT_IDS = [
    "21ee0d80-64f6-4277-80b9-c734d92a2880",  # torzsi
    "e970f4bb-e5f7-4cc5-abc5-8afb52e435bf",  # xertioN
    "4ae11fb8-2077-4078-b7c6-ebbb0e005e9c",  # sycrone
    "12f88420-2da0-497e-92bd-8270d246b428",  # siuhy
    "0cf9c7b8-2003-404d-97d4-fa9b5fae1878",  # Jimpphat
    "84cd6984-6582-483e-bedb-4ea8366c67fb",  # Brollan
    "a6e8dc26-039b-4c8e-96d6-94dcd6b273e9",  # Spinx
    "00193a09-851e-485d-aac2-0f1568e3bbf7",  # TOBIZ
    "fb1af1d5-eee3-46e3-9939-01df9420b9d6",  # xelex
    "80335b52-df57-495f-b197-5741bfbfefbf",  # blick
    "c84f9dbb-69e3-4d31-a283-21516bfe9a3d",  # cliqq
    "81c00116-0f95-4fb5-9e22-e1132f38ebab",  # Jorko
    "d69cd798-afaf-4892-b3bd-2e3bfeb21e7e",  # Joey
]


def timestamp():
    return (
        datetime.datetime.utcnow()
        .replace(tzinfo=datetime.timezone.utc)
        .replace(microsecond=0)
        .isoformat()
    )


def discord_message(message, url):
    if "timestamp" not in message:
        message["timestamp"] = timestamp()

    requests.post(
        url, data=json.dumps(message), headers={"Content-Type": "application/json"}
    )


def discord_message_media(msg):
    discord_message(msg, DISCORD_WEBHOOK_MEDIA_LINKS)


VAMOUZ_AD = {
    "name": "_ _",
    "value": "Check out [vamouz.de](https://vamouz.de) to stay in the loop!",
}

MEDIA_MSG = {
    "avatar_url": "https://cdn.discordapp.com/avatars/884877489376215040/8980dea6629950522895a40cdac45a58.webp?size=640",
    "username": "xertioNpeek",
    "content": "** NEW CONTENT! **",
    "embeds": [
        {
            "title": "**ROPZ - CS:GO Fragmovie by silent**",
            "color": 0xFF0000,
            "author": {
                "name": "vamouz.de",
                "url": "https://vamouz.de",
                "icon_url": "https://vamouz.de/static/home/mouz_small.png",
            },
            "thumbnail": {"url": "https://vamouz.de/static/home/mouzz.png"},
            "fields": [
                {
                    "name": ":link: Link",
                    "value": "[https://www.youtube.com/watch?v=Q5RgA8Pq5Zk](https://www.youtube.com/watch?v=Q5RgA8Pq5Zk)",
                },
                {"name": ":slight_smile: User", "value": "monkii", "inline": True},
                {"name": ":label: Tags", "value": "", "inline": True},
            ],
            "footer": {"icon_url": "https://vamouz.de/static/home/mouzz.png"},
        }
    ],
}

MATCH_MSG = {
    "avatar_url": "https://cdn.discordapp.com/avatars/884877489376215040/8980dea6629950522895a40cdac45a58.webp?size=640",
    "username": "xertioNpeek",
    "content": "** Match live in 30 minutes! ** <@&877296754884755536>",
    "embeds": [
        {
            "title": "",
            "color": 0xFF0000,
            "author": {
                "name": "vamouz.de",
                "url": "https://vamouz.de",
                "icon_url": "https://vamouz.de/static/home/mouz_small.png",
            },
            "thumbnail": {"url": "https://vamouz.de/static/home/mouzz.png"},
            "fields": [
                {"name": ":clock4: Time", "value": "18:00 CEST", "inline": True},
                {
                    "name": ":trophy: Event",
                    "value": "[Flashpoint 3](https://hltv.org/events/5554/vamouz)",
                    "inline": True,
                },
                {
                    "name": ":link: HLTV",
                    "value": "[https://www.hltv.org/matches/2348422/fnatic-vs-mousesports-flashpoint-3](https://www.hltv.org/matches/2348422/fnatic-vs-mousesports-flashpoint-3)",
                },
                {
                    "name": ":information_source: Info",
                    "value": """ - """.replace("\n\n", "\n")
                    .replace("\n", " -")
                    .replace("*", ""),
                },
            ],
            "footer": {"icon_url": "https://vamouz.de/static/home/mouz.png"},
        }
    ],
}

STREAM_MSG = {
    "avatar_url": "https://cdn.discordapp.com/avatars/884877489376215040/8980dea6629950522895a40cdac45a58.webp?size=640",
    "username": "xertioNpeek",
    "content": "",
    "embeds": [
        {
            "title": "",
            "color": 0xFF0000,
            "author": {
                "name": "vamouz.de",
                "url": "https://vamouz.de",
                "icon_url": "https://vamouz.de/static/home/mouz_small.png",
            },
            "thumbnail": {"url": "https://vamouz.de/static/home/mouzz.png"},
            "fields": [
                {
                    "name": ":link: Twitch",
                    "value": "[https://twitch.tv](https://twitch.tv)",
                },
            ],
            "footer": {"icon_url": "https://vamouz.de/static/home/mouz.png"},
        }
    ],
}


def send_stream_message(stream, title, preview):
    msg = copy.deepcopy(STREAM_MSG)
    msg["embeds"][0]["title"] = "{} is live!".format(stream.user_name)
    msg["embeds"][0]["description"] = title
    msg["embeds"][0]["fields"][0]["value"] = "[{}]({})".format(
        "twitch.tv/{}".format(stream.user_name),
        "https://twitch.tv/{}".format(stream.user_name),
    )
    msg["embeds"][0]["image"] = {}
    msg["embeds"][0]["image"]["url"] = preview

    discord_message(msg, DISCORD_WEBHOOK_STREAM_ALERT)


def send_content_message(content):
    msg = copy.deepcopy(MEDIA_MSG)
    msg["embeds"][0]["title"] = content.name
    msg["embeds"][0]["fields"][0]["value"] = "[{}]({})".format(content.url, content.url)
    msg["embeds"][0]["fields"][1]["value"] = content.user.username
    tags = [t.name for t in content.tags.all()]
    msg["embeds"][0]["fields"][2]["value"] = ", ".join(tags)
    discord_message(msg, DISCORD_WEBHOOK_MEDIA_LINKS)


def send_faceit_message(content):
    msg = copy.deepcopy(STREAM_MSG)
    faceit_id = content.get("id")
    if rs.sismember("faceit", faceit_id):
        logger.warning("FACEIT match was already posted {}".format(faceit_id))
        return
    else:
        rs.sadd("faceit", faceit_id)
    team_a = content.get("teams")[0]
    team_b = content.get("teams")[1]
    scraper = cloudscraper.create_scraper()
    STREAM_CHECK_URL = "https://api.faceit.com/stream/v1/streamings?userId={}"
    streams = {}
    for p in team_a.get("roster", []):
        try:
            logger.warning(STREAM_CHECK_URL.format(p.get("id")))
            r = scraper.get(STREAM_CHECK_URL.format(p.get("id")))
            data = r.json()
            logger.warning(data)
            if data.get("payload") and data.get("payload")[0].get("stream"):
                logger.warning(data.get("payload")[0].get("stream"))
                streams[p.get("id")] = (
                    data.get("payload")[0].get("stream", {}).get("channelUrl")
                )
        except Exception as e:
            logger.warning(e)
            continue
    for p in team_b.get("roster", []):
        try:
            logger.warning(STREAM_CHECK_URL.format(p.get("id")))
            r = scraper.get(STREAM_CHECK_URL.format(p.get("id")))
            data = r.json()
            logger.warning(data)
            if data.get("payload") and data.get("payload")[0].get("stream"):
                logger.warning(data.get("payload")[0].get("stream"))
                streams[p.get("id")] = (
                    data.get("payload")[0].get("stream", {}).get("channelUrl")
                )
        except Exception as e:
            logger.warning(e)
            continue
    msg["embeds"][0]["fields"] = []
    team_a_vals = []
    for p in team_a.get("roster", []):
        base = ""
        if p.get("id") in MOUZ_FACEIT_IDS:
            base += "**{}** <:newmouz:898253310312738856>".format(
                escape_markdown(p.get("nickname", ""))
            )
        else:
            base += escape_markdown(p.get("nickname", ""))
        if p.get("id") in streams:
            base += " | [{}]({})".format("Twitch", streams.get(p.get("id")))
        team_a_vals.append(base)
    team_b_vals = []
    for p in team_b.get("roster", []):
        base = ""
        if p.get("id") in MOUZ_FACEIT_IDS:
            base += "**{}** <:newmouz:898253310312738856>".format(
                escape_markdown(p.get("nickname", ""))
            )
        else:
            base += escape_markdown(p.get("nickname", ""))
        if p.get("id") in streams:
            base += " | [{}]({})".format("Twitch", streams.get(p.get("id")))
        team_b_vals.append(base)
    msg["embeds"][0]["fields"].append(
        {"name": team_a.get("name"), "value": "\n".join(team_a_vals), "inline": True}
    )
    msg["embeds"][0]["fields"].append(
        {"name": team_b.get("name"), "value": "\n".join(team_b_vals), "inline": True}
    )
    link_value = "[{}]({})".format(
        "FACEIT Match", "https://faceit.com/en/csgo/room/{}".format(content.get("id"))
    )
    link_value += " | **[{}]({})**".format(
        content.get("entity", {}).get("name"),
        "https://www.faceit.com/en/{}/{}/vamouz".format(
            content.get("entity", {}).get("type"), content.get("entity", {}).get("id")
        ),
    )
    msg["embeds"][0]["fields"].append({"name": ":link: FACEIT", "value": link_value})
    discord_message(msg, DISCORD_WEBHOOK_FACEIT_ALERT)


def send_match_message(match):
    logger.warning("match noti {}".format(str(match)))
    if match.noti_counter > 1:
        return
    if timezone.now() > match.date + timezone.timedelta(minutes=15):
        return
    if match.postponed:
        return
    diff = timezone.now() - match.date
    diff_s = abs(diff.total_seconds())
    msg = copy.deepcopy(MATCH_MSG)
    start_soon = diff_s < 1200
    if match.noti_counter == 1 and not start_soon:
        return
    tag_msg = ""
    if start_soon:
        tag_msg = " <@&932316815567245342>"
        if match.game == "csgo":
            tag_msg = tag_msg + " <@&877296754884755536>"
            if match.teams.filter(team__id=2).exists():
                tag_msg = tag_msg + " <@&1238618270651908198>"
            else:
                tag_msg = tag_msg + " <@&1238618211449180241>"
        elif match.game == "lol":
            tag_msg = tag_msg + " <@&932316936413519942>"
        elif match.game == "valo":
            tag_msg = tag_msg + " <@&941019516119449670>"
        elif match.game == "dota":
            tag_msg = tag_msg + " <@&1217499345323032717>"
    if start_soon:
        msg["content"] = "**MOUZ playing now!** {}".format(tag_msg)
    else:
        msg["content"] = "**MOUZ plays in one hour.** {}".format(tag_msg)
    loc_date = match.date.astimezone(pytz.timezone("Europe/Berlin"))
    msg["embeds"][0]["fields"][0]["value"] = "<t:{}:t>".format(
        int(match.date.timestamp())
    )
    event_name = match.get_event_name()
    if match.game == "csgo":
        msg["embeds"][0]["title"] = str(match) + " <:mouzCS:1238615800278159361>"
        msg["embeds"][0]["fields"][1]["value"] = "[{}]({})".format(
            event_name, match.get_event_url()
        )
    elif match.game == "valo":
        msg["embeds"][0]["title"] = str(match) + " <:mouzValorant:941019392995651585>"
        msg["embeds"][0]["fields"][1]["value"] = "[{}]({})".format(
            event_name, match.event_url
        )
        msg["embeds"][0]["fields"][2]["name"] = ":link: {}".format(
            match.get_match_link().get("title")
        )
        msg["embeds"][0]["fields"][2]["value"] = match.get_match_link().get("link")
        msg["embeds"][0]["fields"] = msg["embeds"][0]["fields"][:3]
    elif match.game == "lol":
        msg["embeds"][0]["title"] = str(match) + " <:mouzLoL:932308802722734090>"
        msg["embeds"][0]["fields"][1]["value"] = "[{}]({})".format(
            event_name, match.event_url
        )
        if match.pl_id:
            msg["embeds"][0]["fields"][2]["name"] = ":link: Prime League"
            msg["embeds"][0]["fields"][2]["value"] = (
                "https://www.primeleague.gg/matches/{}-vamouz".format(match.pl_id)
            )
        else:
            msg["embeds"][0]["fields"][2]["name"] = ":link: Liquipedia"
            msg["embeds"][0]["fields"][2]["value"] = "[{}]({})".format(
                match.event_url, match.event_url
            )
        msg["embeds"][0]["fields"] = msg["embeds"][0]["fields"][:3]
    elif match.game == "dota":
        msg["embeds"][0]["title"] = str(match) + " <:mouzDota:1217499223856250982>"
        msg["embeds"][0]["fields"][1]["value"] = "[{}]({})".format(
            event_name, match.event_url
        )
        msg["embeds"][0]["fields"][2]["name"] = ":link: JoinDota"
        msg["embeds"][0]["fields"][2]["value"] = (
            "https://www.joindota.com/matches/{}-vamouz".format(match.pl_id)
        )
        msg["embeds"][0]["fields"] = msg["embeds"][0]["fields"][:3]
    if match.game == "csgo":
        if match.hltv_id:
            msg["embeds"][0]["fields"][2]["value"] = (
                "[{}](https://hltv.org/matches/{}/vamouz)".format(
                    str(match), match.hltv_id
                )
            )
        elif match.esea_id:
            msg["embeds"][0]["fields"][2]["name"] = ":link: ESEA"
            msg["embeds"][0]["fields"][2]["value"] = (
                "[https://play.esea.net/match/{}](https://play.esea.net/match/{})".format(
                    match.esea_id, match.esea_id
                )
            )
        elif match.faceit_id:
            msg["embeds"][0]["fields"][2]["name"] = ":link: FACEIT"
            msg["embeds"][0]["fields"][2]["value"] = (
                "[{}](https://www.faceit.com/en/csgo/room/{})".format(
                    str(match), match.faceit_id
                )
            )
        else:
            msg["embeds"][0]["fields"][2]["value"] = "."
        if match.hltv_data:
            msg["embeds"][0]["fields"][3]["value"] = (
                match.hltv_data.get("information", "")
                .replace("\n\n", "\n")
                .replace("\n", " -")
                .replace("*", "")
                + "\nFor **notifications** check <#929374656132186162>"
            )
    if match.stream:
        msg["embeds"][0]["fields"].append(
            {"name": ":tv: Stream", "value": match.stream}
        )

    discord_message(msg, DISCORD_WEBHOOK_MATCH_ALERT)
    if start_soon:
        del msg["content"]
        discord_message(msg, DISCORD_WEBHOOK_FACEIT_ALERT)
    if start_soon:
        match.noti_counter = 2
    else:
        match.noti_counter = 1
    match.save()
