import datetime
import json
import logging
import pytz
from content import models as cmodels
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import F, Max, Q
from django.http import Http404, HttpResponseRedirect
from django.utils import timezone
from django.views.generic import DetailView, ListView, TemplateView
from home.models import (
    BaseEvent, Event, ExtraMatch, Match, Redirect, Site, Team,
)
from huey.contrib.djhuey import HUEY
from ropzpeek.models import LevelServer, LevelServerUser, TwitchStream, Warning
from top20 import models

logger = logging.getLogger(__name__)


def sort_matches(m):
    if isinstance(m, Match):
        return (m.postponed, m.date)
    elif isinstance(m, ExtraMatch):
        return (False, m.starting_at)


class HomeView(TemplateView):
    template_name = "home/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["streams"] = TwitchStream.objects.filter(is_live=True)
        upcoming = list(
            Match.objects.filter(status="upcoming").order_by("postponed", "date")[:12]
        )
        upcoming.extend(
            list(
                ExtraMatch.objects.filter(
                    Q(enabled=True, ending_at__isnull=True)
                    | Q(enabled=True, ending_at__gte=timezone.now())
                )
            )
        )
        upcoming.sort(key=sort_matches)
        context["general_up"] = upcoming
        context["general_fi"] = Match.objects.filter(status="finished").order_by(
            "-date"
        )[:12]
        context["mouz_nxt_up"] = Match.objects.filter(
            game="csgo", teams__team__name="MOUZ NXT", status="upcoming"
        ).order_by("date")[:5]
        context["mouz_nxt_fi"] = Match.objects.filter(
            game="csgo", teams__team__name="MOUZ NXT", status="finished"
        ).order_by("-date")[:5]
        context["mouz_nxt_rank"] = Team.objects.get(hltv_id=11176).rank
        context["mouz_up"] = Match.objects.filter(
            game="csgo", teams__team__name="MOUZ", status="upcoming"
        ).order_by("date")[:5]
        context["mouz_fi"] = Match.objects.filter(
            game="csgo", teams__team__name="MOUZ", status="finished"
        ).order_by("-date")[:5]
        context["mouz_rank"] = Team.objects.get(hltv_id=4494).rank
        context["mouz_lol_up"] = Match.objects.filter(
            game="lol", teams__team__name="MOUZ", status="upcoming"
        ).order_by("date")[:5]
        context["mouz_lol_fi"] = Match.objects.filter(
            game="lol", teams__team__name="MOUZ", status="finished"
        ).order_by("-date")[:5]
        context["mouz_valo_up"] = Match.objects.filter(
            game="valo", teams__team__name="MOUZ", status="upcoming"
        ).order_by("date")[:5]
        context["mouz_valo_fi"] = Match.objects.filter(
            game="valo", teams__team__name="MOUZ", status="finished"
        ).order_by("-date")[:5]
        context["competitions"] = models.Competition.objects.filter(
            visible=True
        ).order_by("starting_at")
        context["events"] = Event.objects.order_by("starting_at")
        context["content"] = cmodels.Content.objects.order_by("-date")[:5]
        context["tnow"] = timezone.now()
        context["in24"] = timezone.now() + timezone.timedelta(hours=24)
        context["in1"] = timezone.now() + timezone.timedelta(hours=1)

        return context


class ResultsView(ListView):
    template_name = "home/results.html"
    model = Match
    paginate_by = 30
    ordering = ["-date"]

    def get_queryset(self):
        queryset = Match.objects.filter(status="finished").order_by("-date")
        game = self.request.GET.get("game")
        if game:
            if game == "cs_main":
                queryset = queryset.filter(game="csgo", teams__team__name="MOUZ")
            elif game == "cs_nxt":
                queryset = queryset.filter(game="csgo", teams__team__name="MOUZ NXT")
            else:
                queryset = queryset.filter(game=game)
        return queryset.distinct().order_by("-date")


class MatchesView(ListView):
    template_name = "home/matches.html"
    model = Match
    paginate_by = 30
    ordering = ["-date"]

    def get_queryset(self):
        queryset = Match.objects.filter(status="upcoming")
        game = self.request.GET.get("game")
        if game:
            if game == "cs_main":
                queryset = queryset.filter(game="csgo", teams__team__name="MOUZ")
            elif game == "cs_nxt":
                queryset = queryset.filter(game="csgo", teams__team__name="MOUZ NXT")
            else:
                queryset = queryset.filter(game=game)
        return queryset.distinct().order_by("postponed", "date")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tnow"] = timezone.now()
        context["in24"] = timezone.now() + timezone.timedelta(hours=24)
        context["in1"] = timezone.now() + timezone.timedelta(hours=1)
        return context


class NewHomeView(TemplateView):
    template_name = "home/new_home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["mouz_up"] = Match.objects.filter(status="upcoming").order_by("date")[
            :10
        ]
        context["mouz_fi"] = Match.objects.filter(status="finished").order_by("-date")[
            :10
        ]
        context["competitions"] = models.Competition.objects.filter(
            visible=True
        ).order_by("starting_at")
        context["events"] = Event.objects.order_by("starting_at")
        context["content"] = cmodels.Content.objects.order_by("-date")[:8]
        context["tnow"] = timezone.now()
        return context


class SiteView(DetailView):
    model = Site
    template_name = "home/site.html"


class MajorView(TemplateView):
    template_name = "home/major.html"


class SadView(TemplateView):
    template_name = "home/sad.html"


class RedirectView(DetailView):
    template_name = "home/redirect.html"
    model = Redirect

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.customized:
            return HttpResponseRedirect(self.object.url)
        return super(RedirectView, self).get(*args, **kwargs)


class RankingView(TemplateView):
    template_name = "home/ranking.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        server_id = self.kwargs.get("discord_id")
        if not server_id:
            server_id = 441534100558446594
        ls = LevelServer.objects.filter(discord_id=server_id).first()
        if not ls:
            raise Http404
        start_date, end_date = None, None
        if self.request.GET.get("start_date"):
            date_str = self.request.GET.get("start_date")
            try:
                start_date = timezone.datetime.strptime(date_str, "%d-%m-%Y")
                start_date = timezone.make_aware(start_date)
                start_date = start_date.replace(hour=0, minute=0)
            except ValueError:
                pass
        if self.request.GET.get("end_date"):
            date_str = self.request.GET.get("end_date")
            try:
                end_date = timezone.datetime.strptime(date_str, "%d-%m-%Y")
                end_date = timezone.make_aware(end_date)
                end_date = end_date.replace(hour=23, minute=59)
            except ValueError:
                pass
        if self.request.GET.get("limit") == "last1":
            start_date = timezone.now() - timezone.timedelta(days=1)
            end_date = None
        elif self.request.GET.get("limit") == "last7":
            start_date = timezone.now() - timezone.timedelta(days=7)
            end_date = None
        elif self.request.GET.get("limit") == "last30":
            start_date = timezone.now() - timezone.timedelta(days=30)
            end_date = None
        elif self.request.GET.get("limit") == "last90":
            start_date = timezone.now() - timezone.timedelta(days=90)
            end_date = None
        if start_date or end_date:
            context["ranking"] = ls.get_ranking_limited_time(
                start_date, end_date
            ).filter(xp_diff__isnull=False)
        else:
            if self.request.GET.get('full', 'false') == 'true':
                context["ranking"] = ls.get_ranking()
            else:
                context["ranking"] = ls.get_ranking()[:100]
        logger.warning(start_date)
        logger.warning(end_date)
        context["start_date"] = start_date
        context["end_date"] = end_date

        return context


class EventView(DetailView):
    template_name = "home/event.html"
    model = Event

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class TeamView(ListView):
    template_name = "home/team.html"
    model = Match
    paginate_by = 20
    ordering = ["-date"]
    team = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.team in [
            "cs2",
            "cs2-nxt",
            "valorant",
            "lol",
            "solo",
            "sim",
            "dota",
        ]:
            raise Http404
        if self.team == "cs2":
            context["up"] = Match.objects.filter(
                game="csgo", teams__team__name="MOUZ", status="upcoming"
            ).order_by("postponed", "date")
            context["fi"] = Match.objects.filter(
                game="csgo", teams__team__name="MOUZ", status="finished"
            ).order_by("-date")
        elif self.team == "cs2-nxt":
            context["up"] = Match.objects.filter(
                game="csgo", teams__team__name="MOUZ NXT", status="upcoming"
            ).order_by("postponed", "date")
            context["fi"] = Match.objects.filter(
                game="csgo", teams__team__name="MOUZ NXT", status="finished"
            ).order_by("-date")
        elif self.team == "lol":
            context["up"] = Match.objects.filter(
                game="lol", teams__team__name__icontains="MOUZ", status="upcoming"
            ).order_by("date")
            context["fi"] = Match.objects.filter(
                game="lol", teams__team__name__icontains="MOUZ", status="finished"
            ).order_by("-date")
        elif self.team == "valorant":
            context["up"] = Match.objects.filter(
                game="valo", teams__team__name="MOUZ", status="upcoming"
            ).order_by("date")
            context["fi"] = Match.objects.filter(
                game="valo", teams__team__name="MOUZ", status="finished"
            ).order_by("-date")
        elif self.team == "dota":
            context["up"] = Match.objects.filter(
                game="dota", teams__team__name="MOUZ", status="upcoming"
            ).order_by("date")
            context["fi"] = Match.objects.filter(
                game="dota", teams__team__name="MOUZ", status="finished"
            ).order_by("-date")
        elif self.team == "solo":
            context["up"] = (
                ExtraMatch.objects.filter(~Q(game="sim"))
                .filter(
                    Q(enabled=True, ending_at__isnull=True)
                    | Q(enabled=True, ending_at__gte=timezone.now())
                )
                .order_by("starting_at")
            )
        elif self.team == "sim":
            context["up"] = (
                ExtraMatch.objects.filter(game="sim")
                .filter(
                    Q(enabled=True, ending_at__isnull=True)
                    | Q(enabled=True, ending_at__gte=timezone.now())
                )
                .order_by("starting_at")
            )
        context["team"] = self.team
        context["in24"] = timezone.now() + timezone.timedelta(hours=24)
        context["tnow"] = timezone.now()
        return context

    def get_queryset(self):
        queryset = super(TeamView, self).get_queryset()
        self.team = self.kwargs.get("team")
        if self.team == "cs2":
            queryset = Match.objects.filter(
                game="csgo", teams__team__name="MOUZ", status="finished"
            )
        elif self.team == "cs2-nxt":
            queryset = Match.objects.filter(
                game="csgo", teams__team__name="MOUZ NXT", status="finished"
            )
        elif self.team == "lol":
            queryset = Match.objects.filter(
                game="lol", teams__team__name__icontains="MOUZ", status="finished"
            )
        elif self.team == "valorant":
            queryset = Match.objects.filter(
                game="valo", teams__team__name="MOUZ", status="finished"
            )
        elif self.team == "dota":
            queryset = Match.objects.filter(
                game="dota", teams__team__name="MOUZ", status="finished"
            )
        elif self.team == "solo" or self.team == "sim":
            queryset = Match.objects.none()
        return queryset.distinct().order_by("-date")


class ScheduledTasks(UserPassesTestMixin, TemplateView):
    template_name = "home/tasks.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tasks = {}
        for task in HUEY.scheduled():
            if not HUEY.is_revoked(task):
                try:
                    m = Match.objects.get(pk=task.kwargs.get("match_id"))
                except:
                    HUEY.revoke(task)
                    continue
                if not tasks.get(m):
                    tasks[m] = []
                tasks[m].append(task)
        context["tasks"] = tasks
        return context

    def test_func(self):
        return self.request.user.is_staff


class WarningsView(LoginRequiredMixin, TemplateView):
    template_name = "home/warnings.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        discord_acc = self.request.user.socialaccount_set.filter(
            provider="discord"
        ).first()
        if discord_acc:
            threshold = timezone.datetime(2022, 2, 4, 0, 0, 0, 0)
            threshold = timezone.make_aware(threshold)
            context["warnings"] = Warning.objects.filter(
                date__gte=threshold,
                user__discord_id=discord_acc.extra_data.get("id", -1),
            ).order_by("-date")
        return context


from .forms import MessageForm
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django_redis import get_redis_connection


@user_passes_test(lambda u: u.is_staff)
def fpeek_message(request):
    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid():
            con = get_redis_connection("default")
            data = json.dumps(form.cleaned_data)
            con.lpush("fpeek_msges", data)
            return HttpResponseRedirect("/fpeekmsg/")
    else:
        form = MessageForm()

    return render(request, "home/fpeekmsg.html", {"form": form})


class WidgetView(TemplateView):
    template_name = "home/widget.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["mouz_up"] = Match.objects.filter(status="upcoming").order_by("date")[
            :10
        ]
        context["mouz_fi"] = Match.objects.filter(status="finished").order_by("-date")[
            :10
        ]
        return context
