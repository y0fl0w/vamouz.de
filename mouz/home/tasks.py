import copy
import random
from content.models import Content
from django.utils import timezone
from home.discord import send_content_message, send_match_message
from huey import crontab
from huey.contrib.djhuey import periodic_task, task


@task()
def send_content_msg(content_id):
    content = Content.objects.get(pk=content_id)
    send_content_message(content)


@task()
def send_match_msg(match_id):
    from home.models import Match
    import redis

    rs = redis.Redis(host="redis", port=6379, db=0)
    with rs.lock(str(match_id), blocking_timeout=120):
        match = Match.objects.get(pk=match_id)
        send_match_message(match)


@task()
def send_faceit_msg(content):
    from home.discord import send_faceit_message

    send_faceit_message(content)


@periodic_task(crontab(minute="*/3"))
def update_current_matches():
    from home.models import Match
    in_two_hours = timezone.now() + timezone.timedelta(hours=2)
    curr_matches = Match.objects.filter(
        game="csgo", status="upcoming", date__lt=in_two_hours
    )
    for m in curr_matches:
        if random.randint(0, 5) == 0:
            m.update_match()

    in_one_hour = timezone.now() + timezone.timedelta(hours=1)
    curr_matches = Match.objects.filter(
        game="csgo", status="upcoming", date__lt=in_one_hour
    )
    for m in curr_matches:
        m.update_match()


@periodic_task(crontab(minute="*/10"))
def update_current_lol_matches():
    from home.models import Match

    in_three_days = timezone.now() + timezone.timedelta(hours=24 * 3)
    curr_matches = Match.objects.filter(
        game__in=["lol", "valorant", "dota"], status="upcoming", date__lt=in_three_days
    )
    for m in curr_matches:
        m.update_match()


@periodic_task(crontab(minute="1", hour="3"))
def reschedule_notis():
    import logging

    logger = logging.getLogger(__name__)
    logger.warning("reschedule_notis")
    from home.models import Match
    from huey.contrib.djhuey import HUEY

    HUEY.flush()
    upcoming = Match.objects.filter(status="upcoming")
    for m in upcoming:
        m.schedule_notifications()


@periodic_task(crontab(minute="*/1"))
def check_for_live_streams():
    import logging
    from django.conf import settings
    from django.utils import timezone
    from home.discord import send_stream_message
    from ropzpeek.models import TwitchStream
    from twitchAPI import Twitch

    logger = logging.getLogger(__name__)
    twitch = Twitch(settings.TWITCH_CLIENT_ID, settings.TWITCH_CLIENT_SECRET)
    twitch.authenticate_app([])

    twitch_streams = TwitchStream.objects.filter(active=True)
    streams_without_id = []
    for ts in twitch_streams:
        if not ts.user_id:
            ts.user_id = int(twitch.get_users(logins=[ts.user_name])["data"][0]["id"])
            ts.save()

    twitch_streams = TwitchStream.objects.filter(active=True)
    live = twitch.get_streams(user_id=[str(u.user_id) for u in twitch_streams])["data"]
    for l in live:
        stream = TwitchStream.objects.filter(user_id=int(l["user_id"])).first()
        if stream:
            stream.user_name = l["user_name"]
            stream.title = l["title"]
            stream.viewers = l["viewer_count"]
            stream.save()
            if not stream.is_live and (
                not stream.last_live_date
                or not stream.last_live_date + timezone.timedelta(minutes=10)
                > timezone.now()
            ):
                logger.warning("{} went live".format(stream.user_name))
                send_stream_message(
                    stream,
                    "{} - {}".format(l["title"], l["game_name"]),
                    l["thumbnail_url"].format(width=1920, height=1080),
                )

            stream.last_live_date = timezone.now()
            stream.is_live = True
            stream.save()
    for not_live in TwitchStream.objects.exclude(
        user_id__in=[int(u["user_id"]) for u in live]
    ):
        not_live.is_live = False
        not_live.save()


@periodic_task(crontab(minute="*/90"))
def fetch_hltv():
    from django.core.management import call_command

    call_command("fetch")


@periodic_task(crontab(minute="*/25"))
def fetch_lol():
    return
    from django.core.management import call_command

    call_command("fetch_lol")


@periodic_task(crontab(minute="*/10"))
def fetch_lol_pl():
    return
    from django.core.management import call_command

    call_command("fetch_lol", "--pl")


@periodic_task(crontab(minute="*/25"))
def fetch_dota():
    from django.core.management import call_command

    call_command("fetch_dota")


@periodic_task(crontab(minute="*/120"))
def fetch_faceit():
    from django.core.management import call_command

    call_command("fetch_faceit")


@periodic_task(crontab(minute="*/15"))
def fetch_valo():
    from django.core.management import call_command

    call_command("fetch_valo")
