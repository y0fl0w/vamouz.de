import logging
import pytz
import re
from django import template
from django.conf import settings
from django.utils.timezone import is_aware, make_aware

register = template.Library()

logger = logging.getLogger(__name__)


@register.filter
def setting(name):
    attr = getattr(settings, name, "")
    return attr


@register.filter(name="pretty_number", is_safe=False)
def pretty_number(val, precision=1):
    if not val:
        return None
    try:
        int_val = int(val)
    except ValueError:
        raise template.TemplateSyntaxError(
            f"Value must be an integer. {val} is not an integer"
        )
    if int_val < 1000:
        return str(int_val)
    elif int_val < 1_000_000:
        return f"{ int_val/1000.0:.{precision}f}".rstrip("0").rstrip(".") + "k"
    else:
        return f"{int_val/1_000_000.0:.{precision}f}".rstrip("0").rstrip(".") + "m"


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context["request"].GET.copy()
    for k, v in query.items():
        if kwargs.get(k):
            query[k] = str(kwargs.get(k))
    for k, v in kwargs.items():
        if not query.get(k):
            query[k] = str(kwargs.get(k))
    return query.urlencode()


@register.filter()
def as_berlin(date):
    return make_aware(date, timezone=pytz.utc)


@register.filter()
def remove_emojis(msg):
    return re.sub(":[^\s]*:", "", msg)
