from . import models
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.db.models import JSONField
from django.forms import ModelForm
from django_json_widget.widgets import JSONEditorWidget


class MyUserAdmin(UserAdmin):
    list_filter = UserAdmin.list_filter + ("groups__name",)


class MatchTeamInline(admin.TabularInline):
    model = models.MatchTeam
    extra = 0


class MatchAdminForm(admin.ModelAdmin):
    inlines = (MatchTeamInline,)
    list_filter = ["game"]
    list_display = [
        "__str__",
        "game",
        "status",
        "stream",
        "date",
        "postponed",
        "locked",
    ]
    ordering = ["-date"]


class MatchTeamAdminForm(admin.ModelAdmin):
    pass


class TeamAdminForm(admin.ModelAdmin):
    pass


class RankAdminForm(admin.ModelAdmin):
    pass


class ExtraMatchAdminForm(admin.ModelAdmin):
    list_display = [
        "__str__",
        "enabled",
    ]


class RedirectAdminForm(admin.ModelAdmin):
    pass


class SiteAdminForm(admin.ModelAdmin):
    pass


class EventAdminForm(admin.ModelAdmin):
    formfield_overrides = {
        JSONField: {"widget": JSONEditorWidget},
    }


admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
admin.site.register(models.Match, MatchAdminForm)
admin.site.register(models.ExtraMatch, ExtraMatchAdminForm)
admin.site.register(models.MatchTeam, MatchTeamAdminForm)
admin.site.register(models.Team, TeamAdminForm)
admin.site.register(models.Rank, RankAdminForm)
admin.site.register(models.Redirect, RedirectAdminForm)
admin.site.register(models.Event, EventAdminForm)
admin.site.register(models.Site, SiteAdminForm)
