"""mouz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from . import views
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

app_name = "home"
urlpatterns = [
    path("", views.HomeView.as_view(), name="home"),
    path("results/", views.ResultsView.as_view(), name="results"),
    path("matches/", views.MatchesView.as_view(), name="matches"),
    path("major/", views.MajorView.as_view(), name="major"),
    path("pain/", views.SadView.as_view(), name="sad"),
    path("warnings/", views.WarningsView.as_view(), name="warnings"),
    path("ranking/<int:discord_id>", views.RankingView.as_view(), name="ranking"),
    path("event/<slug:slug>", views.EventView.as_view(), name="event"),
    path("ranking/", views.RankingView.as_view(), name="ranking"),
    path("tasks/", views.ScheduledTasks.as_view(), name="tasks"),
    path("fpeekmsg/", views.fpeek_message, name="fpeekmsg"),
    path("matchwidget/", views.WidgetView.as_view(), name="widget"),
    path("<slug:slug>/", views.SiteView.as_view(), name="site"),
    path("r/<slug:slug>/", views.RedirectView.as_view(), name="redirect"),
    path("team/<slug:team>/", views.TeamView.as_view(), name="team"),
]
