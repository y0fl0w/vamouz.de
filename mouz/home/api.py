from django.contrib.auth.models import User
from django.urls import include, path
from django_filters.rest_framework import DjangoFilterBackend
from home.models import Match, MatchTeam
from rest_framework import mixins, routers, serializers, viewsets


# Serializers define the API representation.
class MatchTeamSerializer(serializers.ModelSerializer):
    team = serializers.StringRelatedField()

    class Meta:
        model = MatchTeam
        fields = ["team", "score"]


class MatchSerializer(serializers.ModelSerializer):
    teams = MatchTeamSerializer(many=True, read_only=True)
    event_url = serializers.CharField(source="get_event_url")
    event_name = serializers.CharField(source="get_event_name")
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return obj.get_match_link().get("link")

    class Meta:
        model = Match
        fields = [
            "id",
            "date",
            "date_guessed",
            "teams",
            "game",
            "url",
            "event_name",
            "event_url",
        ]


# ViewSets define the view behavior.
class MatchViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ["status", "game"]

    def get_queryset(self):
        qs = Match.objects.all()
        status = self.request.query_params.get("status")
        if status:
            qs = qs.filter(status=status)
        if status == "finished":
            qs = qs.order_by("-date")
        if status == "upcoming":
            qs = qs.order_by("date")
        return qs
