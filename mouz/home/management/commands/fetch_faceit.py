import cloudscraper
import json
import pytz
import random
import requests
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from django.conf import settings
from home import models

headers = {
    "Authorization": "Bearer {}".format(settings.FACEIT_BEARER),
}

URL = "https://www.faceit.com/api/championships/v1/matches?participantId=d2865879-5855-49db-a839-1df88f791ee0&participantType=TEAM&championshipId=ef53d445-8731-4945-af31-097548ccba88&limit=20&offset=0&sort=ASC"

MATCH_URL = "https://open.faceit.com/data/v4/matches/{}"

class Command(BaseCommand):
    def handle_matches(self, team, matches):
        for m in matches:
            faceit_id = m.get("origin").get("id", "-1")
            match = models.Match.objects.filter(faceit_id=faceit_id).first()
            if match and match.locked:
                continue
            if not match:
                match = models.Match()
                match.faceit_id = faceit_id
                match.save()
            match_team_a = match.teams.filter(slot="a").first()
            if not match_team_a:
                match_team_a = models.MatchTeam()
                match_team_a.match = match
                match_team_a.team = team
                match_team_a.slot = "a"
                match_team_a.save()
            match_team_b = match.teams.filter(slot="b").first()
            if not match_team_b:
                match_team_b = models.MatchTeam()
                match_team_b.match = match
                team_bm = None
                r = requests.get(MATCH_URL.format(faceit_id), headers=headers)
                print(headers)
                print(MATCH_URL.format(faceit_id))
                if r.status_code == 200:
                    teams = r.json().get('teams')
                    team_b = [value for value in teams.values() if value.get("faction_id") != 'd2865879-5855-49db-a839-1df88f791ee0'][0]
                    team_bm = models.Team()
                    team_bm.name = team_b.get('name')
                    team_bm.save()
                    match_team_b.team = team_bm
                    match_team_b.slot = "b"
                    match_team_b.save()
                else:
                    print(r.status_code)
            match.event_name = "ESEA Advanced S52"
            match.event_url = "https://www.faceit.com/en/cs2/league/ESEA%20League/a14b8616-45b9-4581-8637-4dfd0b5f6af8/2f203265-fc76-4ce5-98a3-6fd1f4717c14/placements"
            if m.get("status") == "finished":
                match.status = "finished"
            starting_at = m.get("origin").get("schedule", "-1")
            if starting_at:
                date = datetime.utcfromtimestamp(int(starting_at) / 1000).replace(
                    tzinfo=pytz.utc
                )
                match.date = date
                match.save()

    def handle(self, *args, **options):
        r = requests.get(URL)
        data = r.json().get("payload")
        team = models.Team.objects.get(hltv_id=11176)
        self.handle_matches(
            team,
            data.get("items", []),
        )
