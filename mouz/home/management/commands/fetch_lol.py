import cloudscraper
import json
import pytz
import requests
import ssl
from bs4 import BeautifulSoup
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.utils import dateparse, timezone
from home import models
from urllib.request import urlopen

URL = "https://liquipedia.net/leagueoflegends/MOUZ"
URL_PL_UPCOMING = "https://www.primeleague.gg/ajax/list_load?name=matches_live_upcoming&page=1&type=box&a1=&filter%5Bteam1%5D=705&devmode=1&language=de"
URL_PL_RESULTS = "https://www.primeleague.gg/ajax/list_load?name=matches_finished&page=1&type=box&a1=&filter%5Bteam1%5D=705&devmode=1&language=de"


class Command(BaseCommand):
    def handle_matches(self, scraper, team, matches, upcoming=False, lp=False):
        for m in matches:
            # find existing match
            match = None
            starting_at = m.get("date")
            one_day_earlier = starting_at - timezone.timedelta(days=1)
            one_day_later = starting_at + timezone.timedelta(days=1)

            if not lp:
                match = models.Match.objects.filter(pl_id=m.get("pl_id", -1)).first()
            if not match:
                match = models.Match.objects.filter(
                    teams__team__name=m.get("team_a"),
                    date__range=(one_day_earlier, one_day_later),
                )
                if match:
                    match = match.filter(teams__team__name=m.get("team_b")).first()
                    if match:
                        print(match)
            if not match:
                match = models.Match()
                match.game = "lol"
                if m.get("pl_id"):
                    match.pl_id = m.get("pl_id")
                match.date = timezone.now()
            elif match.locked:
                continue
            schedule = not match.has_notifications()
            if starting_at and not lp:
                date = starting_at
                if match.date != date:
                    schedule = True
                match.date = date
            match.save()
            if schedule:
                match.reschedule()
            if not upcoming or match.status == "finished":
                match.status = "finished"
            else:
                match.status = "upcoming"
            if m.get("event"):
                match.event_name = m.get("event")
            if lp:
                match.event_url = "https://liquipedia.net" + m.get("event_url")
            match.save()
            match_team_a = match.teams.filter(slot="a").first()
            match_team_b = match.teams.filter(slot="b").first()
            if not match_team_a:
                match_team_a = models.MatchTeam()
                team_a_name = m.get("team_a")
                team_a = models.Team.objects.filter(name=team_a_name).first()
                if not team_a:
                    team_a = models.Team()
                    team_a.name = team_a_name
                    team_a.save()
                match_team_a.match = match
                match_team_a.team = team_a
                match_team_a.slot = "a"
                match_team_a.save()
            if match_team_a.score == 0:
                match_team_a.score = m.get("team_a_score", 0)
                match_team_a.save()
            if not match_team_b:
                if not match_team_b:
                    match_team_b = models.MatchTeam()
                team_b_name = m.get("team_b")
                team_b = models.Team.objects.filter(name=team_b_name).first()
                if not team_b:
                    team_b = models.Team()
                    team_b.name = team_b_name
                    team_b.save()
                match_team_b.match = match
                match_team_b.team = team_b
                match_team_b.slot = "b"
                match_team_b.save()

            if match_team_b.score == 0:
                match_team_b.score = m.get("team_b_score", 0)
                match_team_b.save()

    def add_arguments(self, parser):
        parser.add_argument(
            "--pl",
            action="store_true",
            help="fetch via primeleague.gg",
        )

    def handle(self, *args, **options):
        if options["pl"]:
            scraper = cloudscraper.create_scraper()
            r = scraper.get(URL_PL_UPCOMING)
            soup = BeautifulSoup(json.load(r).get("data"), "html.parser")
            ms = soup.find_all("div", {"class": "widget-match"})
            matches = []
            for m in ms:
                parts = m.find("ul").find_all("li")
                team_a_name = parts[0].text
                team_b_name = parts[2].text
                date = None
                try:
                    date = parts[1].find("span", {"class": "tztime"}).get("data-time")
                except:
                    pass
                if not date:
                    try:
                        date = (
                            parts[1].find("span", {"class": "itime"}).get("data-time")
                        )
                    except:
                        pass
                if not date:
                    continue
                date = datetime.utcfromtimestamp(int(date)).replace(tzinfo=pytz.utc)
                pl_id = int(
                    m.find("a", {"class": "widget-block"})["href"]
                    .split("/")[-1]
                    .split("-")[0]
                )
                if team_b_name == "MOUZ":
                    team_a_name, team_b_name = team_b_name, team_a_name
                matches.append(
                    {
                        "team_a": team_a_name,
                        "team_b": team_b_name,
                        "date": date,
                        "pl_id": pl_id,
                    }
                )
            team = models.Team.objects.get(name="MOUZ")
            self.handle_matches(
                scraper,
                team,
                matches,
                upcoming=True,
                lp=False,
            )
        else:
            scraper = cloudscraper.create_scraper()
            r = scraper.get(URL)
            soup = BeautifulSoup(r.content, "html.parser")
            upcoming = soup.find_all("table", {"class": "infobox_matches_content"})
            matches = []
            for m in upcoming:
                team_a = m.find("td", {"class": "team-left"})
                team_a_name = team_a.find("span", {"class": "team-template-text"}).text
                team_b = m.find("td", {"class": "team-right"})
                team_b_name = team_b.find("span", {"class": "team-template-text"}).text
                if team_b_name == "MOUZ":
                    temp = team_a_name
                    team_a_name = "MOUZ"
                    team_b_name = temp
                event = m.find("td", {"class": "match-filler"})
                event_name = event.find("div").find("div").find("a").text
                event_url = event.find("div").find("div").find("a")["href"]
                match_date = m.find(
                    "span", {"class": "timer-object timer-object-countdown-only"}
                )["data-timestamp"]
                match_date = datetime.utcfromtimestamp(int(match_date)).replace(
                    tzinfo=pytz.utc
                )
                matches.append(
                    {
                        "team_a": team_a_name,
                        "team_b": team_b_name,
                        "event": event_name,
                        "event_url": event_url,
                        "date": match_date,
                    }
                )

            results = []
            m_results = (
                soup.find("div", {"class": "recent-matches"})
                .find("tbody")
                .find_all("tr")
            )
            for i, r in enumerate(m_results):
                if i == 0 or i == 6 or isinstance(r, dict):
                    continue
                rows = r.find_all("td")
                team_a_name = "MOUZ"
                team_b_name = rows[len(rows) - 1].find_all("a")[2].text
                result = rows[len(rows) - 2].text
                result = [int(s) for s in result.split() if s.isdigit()]
                team_a_score = result[0]
                team_b_score = result[1]
                event_name = rows[len(rows) - 3].text
                event_url = rows[len(rows) - 3].find("a")["href"]
                match_date = rows[0].text + "T" + rows[1].text.replace(" UTC", "") + "Z"
                match_date = dateparse.parse_datetime(match_date)
                results.append(
                    {
                        "team_a": team_a_name,
                        "team_b": team_b_name,
                        "team_a_score": team_a_score,
                        "team_b_score": team_b_score,
                        "result": result,
                        "event": event_name,
                        "event_url": event_url,
                        "date": match_date,
                    }
                )
            team = models.Team.objects.get(name="MOUZ")
            self.handle_matches(
                scraper,
                team,
                results,
                upcoming=False,
                lp=True,
            )
            self.handle_matches(
                scraper,
                team,
                matches,
                upcoming=True,
                lp=True,
            )
