from django.core.management.base import BaseCommand, CommandError
from home.models import Match
from huey.contrib.djhuey import HUEY


class Command(BaseCommand):
    def handle(self, *args, **options):
        for task in HUEY.scheduled():
            if not HUEY.is_revoked(task):
                m = Match.objects.get(pk=task.kwargs.get("match_id"))
                print(m)
                print(task)
