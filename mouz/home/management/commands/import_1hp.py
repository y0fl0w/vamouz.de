import cloudscraper
import json
import pytz
import random
import requests
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from ropzpeek import models

BASE_URL = "https://mee6.xyz/api/plugins/levels/leaderboard/441534100558446594?page={}"


class Command(BaseCommand):
    def handle(self, *args, **options):
        i = 0
        r = requests.get(BASE_URL.format(i))
        data = r.json()
        server = models.LevelServer.objects.get(discord_id=441534100558446594)
        lsusers = server.levelserveruser_set.all()
        print(lsusers)
        players = []
        while data.get("players", []):
            i = i + 1
            players.extend(data.get("players"))
            r = requests.get(BASE_URL.format(i))
            data = r.json()
        lsusers.delete()
        for p in players:
            existing_user = lsusers.filter(discord_id=p.get("id", -1)).first()
            if not existing_user:
                user = models.LevelServerUser()
                user.server = server
                user.discord_id = p.get("id")
                user.name = p.get("username")
                user.save()
                existing_user = user
            snap = models.LevelSnapshot()
            snap.user = existing_user
            snap.date = timezone.now()
            snap.xp_count = p.get("xp")
            snap.msg_count = p.get("message_count")
            snap.save()
