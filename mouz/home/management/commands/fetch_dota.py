import cloudscraper
import json
import pytz
import requests
import ssl
from bs4 import BeautifulSoup
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.utils import dateparse, timezone
from home import models
from urllib.request import urlopen

URL_PL_UPCOMING = "https://www.joindota.com/ajax/list_load?module=coverage_match&mode=current&size=&page=1&search=&filters=%7B%22date%22%3A%22_%22%2C%22team1%22%3A%225487%22%2C%22subsite%22%3A%22joindota%22%7D&language=en"
URL_PL_RESULTS = "https://www.joindota.com/ajax/list_load?module=coverage_match&mode=completed&size=&page=1&search=&filters=%7B%22date%22%3A%22_%22%2C%22team1%22%3A%225487%22%2C%22subsite%22%3A%22joindota%22%7D&language=en"


class Command(BaseCommand):
    def handle_matches(self, scraper, team, matches, upcoming=False, lp=False):
        for m in matches:
            # find existing match
            match = None
            starting_at = m.get("date")
            one_day_earlier = starting_at - timezone.timedelta(days=1)
            one_day_later = starting_at + timezone.timedelta(days=1)

            if not lp:
                match = models.Match.objects.filter(pl_id=m.get("pl_id", -1)).first()
            if not match:
                match = models.Match.objects.filter(
                    game="dota",
                    teams__team__name=m.get("team_a"),
                    date__range=(one_day_earlier, one_day_later),
                )
                if match:
                    match = match.filter(teams__team__name=m.get("team_b")).first()
            if not match:
                match = models.Match()
                match.game = "dota"
                if m.get("pl_id"):
                    match.pl_id = m.get("pl_id")
                match.date = timezone.now()
            elif match.locked:
                continue
            schedule = not match.has_notifications()
            if starting_at and not lp:
                date = starting_at
                if match.date != date:
                    schedule = True
                match.date = date
            match.save()
            if schedule:
                match.reschedule()
            if not upcoming or match.status == "finished":
                match.status = "finished"
            else:
                match.status = "upcoming"
            if m.get("event"):
                match.event_name = m.get("event")
            if lp:
                match.event_url = "https://liquipedia.net" + m.get("event_url")
            match.save()
            match_team_a = match.teams.filter(slot="a").first()
            match_team_b = match.teams.filter(slot="b").first()
            if not match_team_a:
                match_team_a = models.MatchTeam()
                team_a_name = m.get("team_a")
                team_a = models.Team.objects.filter(name=team_a_name).first()
                if not team_a:
                    team_a = models.Team()
                    team_a.name = team_a_name
                    team_a.save()
                match_team_a.match = match
                match_team_a.team = team_a
                match_team_a.slot = "a"
                match_team_a.save()
            if match_team_a.score == 0:
                match_team_a.score = m.get("team_a_score", 0)
                match_team_a.save()
            if not match_team_b:
                if not match_team_b:
                    match_team_b = models.MatchTeam()
                team_b_name = m.get("team_b")
                team_b = models.Team.objects.filter(name=team_b_name).first()
                if not team_b:
                    team_b = models.Team()
                    team_b.name = team_b_name
                    team_b.save()
                match_team_b.match = match
                match_team_b.team = team_b
                match_team_b.slot = "b"
                match_team_b.save()

            if match_team_b.score == 0:
                match_team_b.score = m.get("team_b_score", 0)
                match_team_b.save()

    def add_arguments(self, parser):
        parser.add_argument(
            "--pl",
            action="store_true",
            help="fetch via primeleague.gg",
        )

    def handle(self, *args, **options):
        scraper = cloudscraper.create_scraper()
        r = scraper.get(URL_PL_UPCOMING)
        soup = BeautifulSoup(r.json().get("html"), "html.parser")

        ms = soup.find("table").find_all("tr", {"class": "upcoming"})
        matches = []
        for m in ms:
            parts = m.find_all("td")
            team_a_name = parts[0].find("a").find("span").text
            team_b_name = parts[2].find("a").find("span").text
            date = None
            try:
                date = parts[1].find("span", {"class": "tztime"}).get("data-time")
            except:
                pass
            if not date:
                try:
                    date = parts[1].find("span", {"class": "itime"}).get("data-time")
                except:
                    pass
            if not date:
                continue
            date = datetime.utcfromtimestamp(int(date)).replace(tzinfo=pytz.utc)
            pl_id = int(parts[1].find("a")["href"].split("/")[-1].split("-")[0])
            if team_b_name == "MOUZ":
                team_a_name, team_b_name = team_b_name, team_a_name
            matches.append(
                {
                    "team_a": team_a_name,
                    "team_b": team_b_name,
                    "date": date,
                    "pl_id": pl_id,
                }
            )
        team = models.Team.objects.get(name="MOUZ")
        self.handle_matches(
            scraper,
            team,
            matches,
            upcoming=True,
            lp=False,
        )
