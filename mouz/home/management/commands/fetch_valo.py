import cloudscraper
import json
import pytz
import requests
from bs4 import BeautifulSoup
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.utils import dateparse, timezone
from home import models

URL = "https://www.vlr.gg/team/matches/8044/mouz/?group={}"
MATCH_URL = "https://vlr.gg/{}/vamouz"
DATE_FORMAT = "%Y/%m/%d%I:%M %p"


class Command(BaseCommand):
    def get_match_data(self, scraper, vlr_id):
        r = scraper.get(MATCH_URL.format(vlr_id))
        soup = BeautifulSoup(r.content, "html.parser")
        header = soup.find("div", {"class": "match-header"})
        event_url = header.find("a", {"class": "match-header-event"})["href"]
        event_url = "https://vlr.gg" + event_url
        event_name = (
            header.find("a", {"class": "match-header-event"})
            .find("div")
            .find("div")
            .text.strip()
        )
        team_a_name = header.find("a", {"class": "mod-1"}).text.strip()
        team_a_id = header.find("a", {"class": "mod-1"})["href"].split("/")[2]
        team_b_name = header.find("a", {"class": "mod-2"}).text.strip()
        team_b_id = header.find("a", {"class": "mod-2"})["href"].split("/")[2]
        score = header.find("div", {"class": "js-spoiler"})
        team_a_score = 0
        team_b_score = 0
        if score:
            team_a_score = int(score.findChildren()[0].text.strip())
            team_b_score = int(score.findChildren()[2].text.strip())
        if team_b_name == "MOUZ":
            team_a_name, team_b_name = team_b_name, team_a_name
            team_a_score, team_b_score = team_b_score, team_a_score
            team_a_id, team_b_id = team_b_id, team_a_id
        return {
            "team_a_name": team_a_name,
            "team_a_id": int(team_a_id),
            "team_b_name": team_b_name,
            "team_b_id": int(team_b_id),
            "team_a_score": team_a_score,
            "team_b_score": team_b_score,
            "event_name": event_name,
            "event_url": event_url,
        }

    def handle_matches(self, scraper, matches, upcoming=False, lp=False):
        for m in matches:
            vlr_id = m.get("vlr_id", "-1")
            vlr_id = int(vlr_id)
            match = models.Match.objects.filter(vlr_id=vlr_id).first()
            if match and match.locked:
                continue
            if not match:
                match = models.Match()
                match.vlr_id = int(m.get("vlr_id", "666666"))
                match.date = timezone.now()
            match.game = "valo"
            if not upcoming:
                match.status = "finished"
            else:
                match.status = "upcoming"
            starting_at = m.get("date")
            schedule = not match.has_notifications()
            if starting_at:
                date = starting_at
                print(date)
                print(match.date)
                if match.date != date:
                    schedule = True
                match.date = date
            match.save()
            if schedule:
                match.reschedule()
            match_team_a = match.teams.filter(slot="a").first()
            match_team_b = match.teams.filter(slot="b").first()
            if (
                match_team_a
                and match_team_b
                and match.event_name
                and match.event_url
                and not (
                    match.status == "finished"
                    and match_team_a.score == 0
                    and match_team_b.score == 0
                )
            ):
                continue
            print("fetch additional data")
            match_data = self.get_match_data(scraper, match.vlr_id)
            match.event_name = match_data.get("event_name")
            match.event_url = match_data.get("event_url")
            if not match_team_a:
                match_team_a = models.MatchTeam()
                team_a_id = match_data.get("team_a_id", "-1")
                team_a_id = int(team_a_id)
                team_a_name = match_data.get("team_a_name", "TBA")
                team_a = models.Team.objects.filter(vlr_id=team_a_id).first()
                if not team_a and team_a_id != -1:
                    team_a = models.Team()
                    team_a.vlr_id = team_a_id
                    team_a.name = team_a_name
                    team_a.save()
                match_team_a.match = match
                match_team_a.team = team_a
                match_team_a.score = match_data.get("team_a_score", 0)
                match_team_a.slot = "a"
            if not match_team_b:
                match_team_b = models.MatchTeam()
                team_b_id = match_data.get("team_b_id", "-1")
                team_b_id = int(team_b_id)
                team_b_name = match_data.get("team_b_name", "TBA")
                team_b = models.Team.objects.filter(vlr_id=team_b_id).first()
                if not team_b and team_b_id != -1:
                    team_b = models.Team()
                    team_b.vlr_id = team_b_id
                    team_b.name = team_b_name
                    team_b.save()
                elif not team_b:
                    team_b = models.Team.objects.filter(name="TBA").first()
                match_team_b.match = match
                match_team_b.team = team_b
                match_team_b.score = match_data.get("team_b_score", 0)
                match_team_b.slot = "b"
            match_team_a.score = match_data.get("team_a_score", 0)
            match_team_b.score = match_data.get("team_b_score", 0)
            match_team_a.save()
            match_team_b.save()
            match.save()

    def add_arguments(self, parser):
        parser.add_argument(
            "--pl",
            action="store_true",
            help="fetch via primeleague.gg",
        )

    def handle(self, *args, **options):
        print("fetch valorant")
        scraper = cloudscraper.create_scraper()
        url = URL.format("completed")
        r = scraper.get(url)
        soup = BeautifulSoup(r.content, "html.parser")
        finished = soup.find_all("a", {"class": "m-item"})
        results = []
        for m in finished[:5]:
            vlr_id = m["href"].split("/")[1]
            date = m.find("div", {"class": "m-item-date"})
            date_string = date.text.replace("\n", "").replace("\t", "")
            date_obj = datetime.strptime(date_string, DATE_FORMAT)
            date_obj = date_obj.astimezone(pytz.timezone("Europe/Berlin"))
            if vlr_id in ["66611", "66604", "66589"]:
                continue
            results.append({"date": date_obj, "vlr_id": int(vlr_id)})
        url = URL.format("upcoming")
        r = scraper.get(url)
        soup = BeautifulSoup(r.content, "html.parser")
        upcoming = soup.find_all("a", {"class": "m-item"})
        matches = []
        for m in upcoming:
            vlr_id = m["href"].split("/")[1]
            date = m.find("div", {"class": "m-item-date"})
            date_string = date.text.replace("\n", "").replace("\t", "")
            date_obj = datetime.strptime(date_string, DATE_FORMAT)
            date_obj = date_obj.astimezone(pytz.timezone("Europe/Berlin"))
            matches.append({"date": date_obj, "vlr_id": int(vlr_id)})
        self.handle_matches(
            scraper,
            results,
            upcoming=False,
        )
        self.handle_matches(
            scraper,
            matches,
            upcoming=True,
        )
