import cloudscraper
import json
import pytz
import random
import requests
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from home import models

BASE_URL = "http://hltv.org/mobile/TeamInfo?teamId={}"
MOUZ_IDS = [4494]
#MOUZ_IDS = [11176, 4494]
MATCH_URL = "http://hltv.org/mobile/MatchInfo?matchId={}"

PROXIES = {
    "http": "http://{}:{}@brd.superproxy.io:33335".format(
        settings.BRIGHTDATA_USER, settings.BRIGHTDATA_PW
    ),
    "https": "http://{}:{}@brd.superproxy.io:33335".format(
        settings.BRIGHTDATA_USER, settings.BRIGHTDATA_PW
    ),
}


class Command(BaseCommand):
    def handle_matches(self, scraper, team, matches, upcoming=False):
        for m in matches:
            hltv_id = m.get("matchId", "-1")
            hltv_id = int(hltv_id)
            match = models.Match.objects.filter(hltv_id=hltv_id).first()
            if match and match.locked:
                continue
            starting_at = m.get("startDateTime")
            if starting_at:
                date = timezone.make_aware(
                    timezone.datetime.fromisoformat(starting_at[:-1]),
                    pytz.timezone("utc"),
                )
                one_day_earlier = date - timezone.timedelta(days=1)
                one_day_later = date + timezone.timedelta(days=1)
                if not match:
                    match = models.Match.objects.filter(
                        teams__team__name=m.get("team_a"),
                        date__range=(one_day_earlier, one_day_later),
                    )
                    if match:
                        match = match.filter(teams__team__name=m.get("team_b")).first()
                        if match:
                            match.hltv_id = hltv_id
                            match.save()
            if not match:
                match = models.Match()
                match.hltv_id = int(m.get("matchId", "666666"))
                match.date = timezone.now()
                team.no_match_msg = None
                team.save()
            if random.randint(0, 49) == 0 or not match.hltv_data:
                url = MATCH_URL.format(hltv_id)
                r = scraper.get(url)
                data = r.json()
                match.hltv_data = data
            if not upcoming:
                match.status = "finished"
            else:
                match.status = "upcoming"
            starting_at = m.get("startDateTime")
            schedule = not match.has_notifications() and not match.status == "finished"
            starting_at = m.get("startDateTime")
            if starting_at:
                date = timezone.make_aware(
                    timezone.datetime.fromisoformat(starting_at[:-1]),
                    pytz.timezone("utc"),
                )
                if match.date != date:
                    schedule = True
                match.date = date
            match.save()
            if schedule:
                match.reschedule()
            match_team_a = match.teams.filter(slot="a").first()
            match_team_b = match.teams.filter(slot="b").first()
            is_tba = False
            if match_team_b:
                if match_team_b.team.name == "TBD":
                    is_tba = True
            if not match_team_a:
                match_team_a = models.MatchTeam()
                team_a_id = m.get("ownTeam", {}).get("teamId", "-1")
                team_a_id = int(team_a_id)
                team_a_name = m.get("ownTeam", {}).get("name", "TBA")
                team_a = models.Team.objects.filter(hltv_id=team_a_id).first()
                if not team_a and team_a_id != -1:
                    team_a = models.Team()
                    team_a.hltv_id = team_a_id
                    team_a.name = team_a_name
                    team_a.save()
                match_team_a.match = match
                match_team_a.team = team_a
                match_team_a.slot = "a"
                match_team_a.save()
            if match_team_a.score == 0:
                match_team_a.score = int(m.get("ownTeamScore", "0"))
                match_team_a.save()
            if not match_team_b or is_tba:
                if not match_team_b:
                    match_team_b = models.MatchTeam()
                team_b_id = m.get("opponentTeam", {})
                if not team_b_id:
                    team_b_id = -1
                else:
                    team_b_id = team_b_id.get("teamId", "-1")
                team_b_id = int(team_b_id)
                team_b_name = m.get("opponentTeam", {})
                if not team_b_name:
                    team_b_name = "TBD"
                else:
                    team_b_name = team_b_name.get("name", "TBD")
                team_b = models.Team.objects.filter(hltv_id=team_b_id).first()
                if not team_b and team_b_id != -1:
                    team_b = models.Team()
                    team_b.hltv_id = team_b_id
                    team_b.name = team_b_name
                    team_b.save()
                elif not team_b:
                    team_b = models.Team.objects.filter(name="TBD").first()
                match_team_b.match = match
                match_team_b.team = team_b
                match_team_b.slot = "b"
                match_team_b.save()

            if match_team_b.score == 0:
                match_team_b.score = int(m.get("opponentTeamScore", "0"))
                match_team_b.save()

    def handle(self, *args, **options):
        scraper = requests.Session()

        scraper.proxies.update(PROXIES)
        for t in MOUZ_IDS:
            url = BASE_URL.format(t)
            r = scraper.get(url)
            data = r.json()
            team = models.Team.objects.get(hltv_id=t)
            team.rank = data.get("worldRank", 99)
            if not team.rank:
                team.rank = 99
            team.save()
            self.handle_matches(
                scraper,
                team,
                data.get("recentMatches", []),
                upcoming=False,
            )
            self.handle_matches(
                scraper,
                team,
                data.get("upcomingMatches", []),
                upcoming=True,
            )
