# Generated by Django 3.1 on 2021-12-10 22:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0016_event"),
    ]

    operations = [
        migrations.AddField(
            model_name="event",
            name="slug",
            field=models.SlugField(default="", max_length=255, unique=True),
            preserve_default=False,
        ),
    ]
