# Generated by Django 3.1 on 2021-06-28 00:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0004_auto_20210628_0233"),
    ]

    operations = [
        migrations.AddField(
            model_name="match",
            name="teams",
            field=models.ManyToManyField(through="home.MatchTeam", to="home.Team"),
        ),
    ]
