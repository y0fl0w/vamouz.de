# Generated by Django 3.1 on 2021-10-18 19:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0013_auto_20211018_2107"),
    ]

    operations = [
        migrations.AlterField(
            model_name="match",
            name="hltv_data",
            field=models.JSONField(blank=True, default=dict, null=True),
        ),
    ]
