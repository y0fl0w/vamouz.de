# Generated by Django 3.1 on 2021-12-10 22:43

import markdownfield.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0018_auto_20211210_2324"),
    ]

    operations = [
        migrations.AddField(
            model_name="event",
            name="desc",
            field=markdownfield.models.MarkdownField(
                blank=True,
                help_text="Event description",
                rendered_field="desc_rendered",
            ),
        ),
        migrations.AddField(
            model_name="event",
            name="desc_rendered",
            field=markdownfield.models.RenderedMarkdownField(default=""),
            preserve_default=False,
        ),
    ]
