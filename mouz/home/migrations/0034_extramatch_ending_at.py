# Generated by Django 3.1 on 2022-03-14 00:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0033_auto_20220226_0213"),
    ]

    operations = [
        migrations.AddField(
            model_name="extramatch",
            name="ending_at",
            field=models.DateTimeField(
                blank=True,
                help_text="Date - if provided it will be used to exclude the event after that date - even if enabled is set",
                null=True,
            ),
        ),
    ]
