# Generated by Django 3.1 on 2022-02-09 15:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0028_auto_20220208_2310"),
    ]

    operations = [
        migrations.AddField(
            model_name="team",
            name="vlr_id",
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
