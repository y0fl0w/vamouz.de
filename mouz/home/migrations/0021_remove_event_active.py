# Generated by Django 3.1 on 2021-12-12 21:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0020_event_active"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="event",
            name="active",
        ),
    ]
