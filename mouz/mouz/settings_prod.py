from mouz.settings import *

DEBUG = False

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "mouz",
        "USER": "mouz",
        "PASSWORD": "test",
        "HOST": "db",
        "PORT": "5432",
    }
}
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/9",
        "OPTIONS": {"CLIENT_CLASS": "django_redis.client.DefaultClient"},
        "KEY_PREFIX": "cache",
    }
}

RQ_QUEUES = {
    "high": {
        "USE_REDIS_CACHE": "default",
    },
    "low": {
        "USE_REDIS_CACHE": "default",
    },
}

CSRF_TRUSTED_ORIGINS = ["https://vamouz.de"]
