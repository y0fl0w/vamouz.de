"""mouz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path
from home.api import MatchViewSet
from rest_framework import routers
from ropzpeek.views import FaceitWebhook, QuoteListView, YoutubeWebhook

router = routers.DefaultRouter()
router.register(r"matches", MatchViewSet, basename="match")

urlpatterns = [
    path("competition/", include("top20.urls")),
    path("content/", include("content.urls")),
    path("user/", include("allauth.urls")),
    path("vamouz/", admin.site.urls),
    path("faceit/", FaceitWebhook.as_view(), name="faceit-endpoint"),
    path("youtube/", YoutubeWebhook.as_view(), name="youtube-endpoint"),
    path("quotes/", QuoteListView.as_view(), name="quotes"),
    path("", include("home.urls")),
    path("api/", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
