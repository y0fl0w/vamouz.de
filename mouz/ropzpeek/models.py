import json
import logging
from datetime import datetime
from django.contrib.auth import get_user_model
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.contrib.postgres.fields import ArrayField
from django.core.cache import cache
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import F, Max, OuterRef, Q, Subquery
from django.db.models.expressions import Window
from django.db.models.functions import Coalesce, Rank
from django.utils import timezone
from django_redis import get_redis_connection

logger = logging.getLogger(__name__)


class Warning(models.Model):
    user = models.ForeignKey(
        "ropzpeek.LevelServerUser",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="infractions",
    )
    warned_by = models.ForeignKey(
        "ropzpeek.LevelServerUser",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="warned",
    )
    date = models.DateTimeField(auto_now_add=True)
    message = models.TextField()

    def __str__(self):
        return "{} - {}, {} - {}".format(
            self.user.server.name, self.user.name, naturaltime(self.date), self.message
        )


class Command(models.Model):
    command = models.CharField(max_length=100)
    reply = models.TextField()
    ttl = models.IntegerField(
        blank=True,
        null=True,
        help_text="if provided, the reply will be deleted after x seconds",
    )
    active = models.BooleanField(
        default=True, help_text="toggle to enable/disable the command"
    )
    channels = ArrayField(
        models.PositiveBigIntegerField(),
        help_text="List of channel ids. If provided the command will trigger only within these channels.",
        blank=True,
    )
    roles = ArrayField(
        models.PositiveBigIntegerField(),
        help_text="List of role ids. If provided the command will trigger only for users having one of these roles.",
        blank=True,
    )
    use_embed = models.BooleanField(
        default=False,
        help_text="Use a embed to send the reply instead of raw msg content",
    )

    def __str__(self):
        return self.command


class LevelServer(models.Model):
    discord_id = models.PositiveBigIntegerField(
        help_text="the server you want to use the level system on"
    )
    # thresholds for random reward inbetween
    min_xp_reward = models.IntegerField(default=15)
    max_xp_reward = models.IntegerField(default=25)
    name = models.CharField(blank=True, null=True, max_length=255)
    time_constraint = models.IntegerField(
        blank=True,
        null=True,
        help_text="If provided only one msg within the given time frame (in seconds) counts",
    )

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.update_user_levels()

    def update_user_levels(self):
        for u in self.levelserveruser_set.all():
            u.update_level()

    def get_ranking(self):
        ranking = cache.get("ranking_{}".format(self.discord_id))
        if not ranking:
            ranking = (
                self.levelserveruser_set.filter()
                .annotate(
                    max_xp=Max("levelsnapshot__xp_count"),
                    msges=Max("levelsnapshot__msg_count"),
                    level=F("current_level__level"),
                    level_name=F("current_level__name"),
                    rank=Window(expression=Rank(), order_by=F("max_xp").desc()),
                    level_min_xp=F("current_level__min_xp"),
                    level_max_xp=F("current_level__max_xp"),
                )
                .filter(
                    msges__isnull=False,
                    max_xp__isnull=False,
                )
                .order_by("-max_xp")
                .values()
            )
            store = json.dumps(list(ranking), cls=DjangoJSONEncoder)
            cache.set("ranking_{}".format(self.discord_id), store, 300)
        else:
            ranking = json.loads(ranking)
        return ranking

    def store_ranking_in_cache(self):
        ranking = self.get_ranking()
        r = get_redis_connection("default")
        for u in ranking:
            r.set(
                "{}_{}".format(self.discord_id, u.get("discord_id")),
                json.dumps(u),
            )

    def get_ranking_limited_time(self, start_time=None, end_time=None):
        if not start_time:
            start_time = LevelSnapshot.objects.filter(user_id=OuterRef("pk")).order_by(
                "date"
            )
        else:
            start_time = LevelSnapshot.objects.filter(
                date__lt=start_time, user_id=OuterRef("pk")
            ).order_by("-date")
        if not end_time:
            end_time = LevelSnapshot.objects.filter(user_id=OuterRef("pk")).order_by(
                "-date"
            )
        else:
            end_time = LevelSnapshot.objects.filter(
                date__lt=end_time, user_id=OuterRef("pk")
            ).order_by("-date")
        return (
            self.levelserveruser_set.filter()
            .annotate(
                start_xp=Coalesce(Subquery(start_time.values("xp_count")[:1]), 0),
                end_xp=Coalesce(Subquery(end_time.values("xp_count")[:1]), 0),
                start_msges=Coalesce(Subquery(start_time.values("msg_count")[:1]), 0),
                end_msges=Coalesce(Subquery(end_time.values("msg_count")[:1]), 0),
                msg_diff=F("end_msges") - F("start_msges"),
                xp_diff=F("end_xp") - F("start_xp"),
            )
            .order_by(F("xp_diff").desc(nulls_last=True))
        )

    def print_ranking_stats(self, start_time=None, end_time=None, limit=None):
        ranking = self.get_ranking_limited_time(start_time, end_time)
        print("MOUZ discord ranking")
        print("Start date: {}".format(start_time))
        print("End date: {}".format(end_time))
        print()
        for u in ranking[:limit]:
            print("{} - XP: {} - MSG: {}".format(u.name, u.xp_diff, u.msg_diff))

    def __str__(self):
        name = self.discord_id
        if self.name:
            name = self.name
        return str(name)


class LevelServerRole(models.Model):
    discord_id = models.PositiveBigIntegerField()
    server = models.ForeignKey(LevelServer, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{} - {}".format(self.name, self.discord_id)


class LevelServerLevel(models.Model):
    level = models.PositiveIntegerField(default=0)
    server = models.ForeignKey(LevelServer, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    # thresholds for a certain level
    min_xp = models.PositiveBigIntegerField()
    max_xp = models.PositiveBigIntegerField()
    role = models.ForeignKey(
        LevelServerRole, on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        return "{} - {}".format(self.server, self.name)


class LevelServerUser(models.Model):
    user = models.ForeignKey(
        get_user_model(), blank=True, null=True, on_delete=models.SET_NULL
    )
    server = models.ForeignKey(LevelServer, on_delete=models.CASCADE)
    discord_id = models.PositiveBigIntegerField()
    name = models.CharField(max_length=255, blank=True, null=True)
    avatar = models.URLField(blank=True, null=True)
    current_level = models.ForeignKey(
        LevelServerLevel, blank=True, null=True, on_delete=models.SET_NULL
    )

    def update_level(self):
        last_snap = self.levelsnapshot_set.order_by("-date").first()
        if last_snap:
            l = self.server.levelserverlevel_set.filter(
                min_xp__lte=last_snap.xp_count, max_xp__gte=last_snap.xp_count
            ).first()
        else:
            l = self.server.levelserverlevel_set.order_by("min_xp").first()
        curr_level = self.current_level
        self.current_level = l
        self.save()
        if not curr_level or curr_level.pk != l.pk:
            self.level_change(curr_level)

    def level_change(self, old):
        # lvl changed from old to self.current_level
        r = get_redis_connection("default")
        return

    def get_level_data(self):
        ranking = self.server.get_ranking()
        for u in ranking:
            if u.get("discord_id") == self.discord_id:
                return u

    def get_activity(self, days=31):
        now = timezone.now()
        date = now - timezone.timedelta(days=days)
        last_ss_before = (
            self.levelsnapshot_set.filter(date__lt=date).order_by("-date").first()
        )
        last_ss = self.levelsnapshot_set.order_by("-date").first()
        if not last_ss:
            return None
        if not last_ss_before:
            return {
                "msges": last_ss.msg_count,
                "xp": last_ss.xp_count,
            }
        else:
            return {
                "msges": last_ss.msg_count - last_ss_before.msg_count,
                "xp": last_ss.xp_count - last_ss_before.xp_count,
            }

    def __str__(self):
        name = self.discord_id
        if self.name:
            name = self.name
        return "{} - {}".format(self.server, name)


class LevelSnapshot(models.Model):
    user = models.ForeignKey(LevelServerUser, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    msg_count = models.PositiveIntegerField()
    xp_count = models.PositiveIntegerField()

    def __str__(self):
        return "{} - {} - msges: {} | XP: {}".format(
            self.user, self.date, self.msg_count, self.xp_count
        )


class TwitchStream(models.Model):
    user_name = models.CharField(
        max_length=255,
        help_text="The twitch username e.g. heromarine for https://twitch.tv/heromarine",
    )
    user_id = models.PositiveIntegerField(
        blank=True,
        null=True,
        help_text="The twitch user id, will be retrieved automatically if not provided",
    )
    active = models.BooleanField(
        default=True, help_text="Notifications get posted in discord if active"
    )
    title = models.CharField(
        max_length=1024, blank=True, null=True, help_text="The current stream title"
    )
    is_live = models.BooleanField(default=False)
    last_live_date = models.DateTimeField(blank=True, null=True)
    viewers = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.user_name


class Quote(models.Model):
    date = models.DateField()
    data = models.JSONField(blank=True, null=True, default=dict)

    def get_date(self):
        date = self.data.get("message_date")
        if date:
            return timezone.make_aware(datetime.fromtimestamp(date))

    def get_link(self):
        return "https://discord.com/channels/{}/{}/{}".format(
            self.data.get("server_id"),
            self.data.get("channel_id"),
            self.data.get("message_id"),
        )

    def __str__(self):
        return "QOTD - {} from {}".format(
            self.date, self.data.get("author_name", "unknown")
        )
