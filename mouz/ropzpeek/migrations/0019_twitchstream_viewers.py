# Generated by Django 3.1 on 2022-03-07 17:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("ropzpeek", "0018_twitchstream_title"),
    ]

    operations = [
        migrations.AddField(
            model_name="twitchstream",
            name="viewers",
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
