from django.apps import AppConfig


class RopzpeekConfig(AppConfig):
    name = "ropzpeek"
