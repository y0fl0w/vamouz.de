from . import models
from django.contrib import admin


class CommandAdminForm(admin.ModelAdmin):
    search_fields = ["command"]


class WarningAdminForm(admin.ModelAdmin):
    autocomplete_fields = ["user", "warned_by"]
    search_fields = ["user__name", "user__discord_id"]


class LevelInline(admin.TabularInline):
    model = models.LevelServerLevel
    extra = 0
    show_change_link = True


class LevelRoleInline(admin.TabularInline):
    model = models.LevelServerRole
    extra = 0
    show_change_link = True


class LevelServerAdminForm(admin.ModelAdmin):
    inlines = [LevelInline]


class LevelServerRoleAdminForm(admin.ModelAdmin):
    pass


class LevelServerLevelAdminForm(admin.ModelAdmin):
    pass


class QuoteAdminForm(admin.ModelAdmin):
    pass


class LevelServerUserAdminForm(admin.ModelAdmin):
    search_fields = ["name"]
    list_display = ["name", "xp_msg"]

    def xp_msg(self, obj):
        snap = obj.levelsnapshot_set.order_by("-date").first()
        if not snap:
            return ""
        return "Messages: {}, XP: {}".format(
            snap.msg_count,
            snap.xp_count,
        )

    def get_form(self, request, obj=None, **kwargs):
        form = super(LevelServerUserAdminForm, self).get_form(request, obj, **kwargs)
        if obj:
            form.base_fields["current_level"].queryset = (
                models.LevelServerLevel.objects.filter(server=obj.server)
            )
        return form


class LevelSnapshotAdminForm(admin.ModelAdmin):
    search_fields = ["user__name"]


class TwitchStreamAdminForm(admin.ModelAdmin):
    search_fields = ["user_name"]
    list_display = ["user_name", "active", "is_live", "last_live_date"]


admin.site.register(models.Command, CommandAdminForm)
admin.site.register(models.LevelServer, LevelServerAdminForm)
admin.site.register(models.LevelServerRole, LevelServerRoleAdminForm)
admin.site.register(models.LevelServerLevel, LevelServerLevelAdminForm)
admin.site.register(models.LevelServerUser, LevelServerUserAdminForm)
admin.site.register(models.LevelSnapshot, LevelSnapshotAdminForm)
admin.site.register(models.Quote, QuoteAdminForm)
admin.site.register(models.TwitchStream, TwitchStreamAdminForm)
admin.site.register(models.Warning, WarningAdminForm)
