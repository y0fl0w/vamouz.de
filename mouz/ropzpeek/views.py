import datetime
import json
import logging
import pytz
import xml.etree.ElementTree as ET
from . import models
from django.conf import settings
from django.db.models import Count, F, Q, Window
from django.db.models.functions import DenseRank
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic import ListView
from home.tasks import send_faceit_msg

FACEIT_WEBHOOK_SECRET = settings.FACEIT_WEBHOOK_SECRET
logger = logging.getLogger(__name__)


class FaceitWebhook(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FaceitWebhook, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        jsondata = request.body
        if not request.META.get("HTTP_VAMOUZ") == FACEIT_WEBHOOK_SECRET:
            logger.warning("post request with missing auth on /faceit/ endpoint")
            return HttpResponse(status=401)
        try:
            data = json.loads(jsondata)
        except json.decoder.JSONDecodeError:
            logger.warning("post request with malformed body on /faceit/ endpoint")
            return HttpResponse(status=400)
        logger.warning("received event on /faceit/ endpoint")
        send_faceit_msg(data.get("payload", {}))
        return HttpResponse(status=200)


class YoutubeWebhook(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(YoutubeWebhook, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponse(request.GET.get("hub.challenge"))

    def post(self, request, *args, **kwargs):
        logger.warning("received event on /youtube/ endpoint")
        xmldata = request.body
        logger.warning(xmldata)
        tree = ET.fromstring(xmldata)
        el = tree.find("{http://www.w3.org/2005/Atom}entry")
        ytubeid = el.find("{http://www.youtube.com/xml/schemas/2015}videoId").text
        logger.warning(ytubeid)
        return HttpResponse(status=200)


class QuoteListView(ListView):
    model = models.Quote
    paginate_by = 20
    ordering = ["-date"]

    def get_queryset(self):
        qs = super().get_queryset()
        search_term = self.request.GET.get("filter", None)
        logger.warning(search_term)
        if search_term is not None:
            qs = qs.filter(
                Q(data__author_name__icontains=search_term)
                | Q(data__message_content__icontains=search_term)
            )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ranking"] = (
            models.Quote.objects.values("data__author_name")
            .annotate(num=Count("pk"))
            .annotate(
                dense_rank=Window(
                    expression=DenseRank(),
                    order_by=[
                        F(
                            "num",
                        ).desc(),
                    ],
                )
            )
            .order_by("dense_rank")
        )
        return context
