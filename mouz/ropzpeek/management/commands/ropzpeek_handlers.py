import discord
import json
import logging
import pytz
import random
import re
from asgiref.sync import sync_to_async
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.db.models import Q
from django.db.models.fields.json import KeyTextTransform
from django.utils import timezone
from home.models import ExtraMatch, Match, Team
from home.templatetags.home import pretty_number
from ropzpeek.models import LevelServer, LevelServerUser, Quote, Warning

logger = logging.getLogger(__name__)


@sync_to_async
def get_upcoming(game=None, team=None):
    if game:
        disable_include = False
        if game in ["sc2", "starcraft", "starcraft2", "sc"]:
            game = "sc2"
        if game in ["csgo", "cs", "cs2"]:
            game = "csgo"
        elif game in ["valo", "valorant", "val", "vlr"]:
            game = "valo"
        elif game in ["dota", "dota2"]:
            game = "dota"
        if team and team in ["nxt", "NXT"]:
            matches = Match.objects.filter(
                game="csgo", status="upcoming", teams__team__name="MOUZ NXT"
            ).order_by("postponed", "date")
            disable_include = True
        elif team and team == "main":
            matches = Match.objects.filter(
                game="csgo", status="upcoming", teams__team__name="MOUZ"
            ).order_by("postponed", "date")
            disable_include = True
        elif game in ["sc2"]:
            matches = ExtraMatch.objects.filter(
                Q(enabled=True, ending_at__isnull=True)
                | Q(enabled=True, ending_at__gte=timezone.now()),
                game=game,
            ).order_by("starting_at")
        else:
            logger.warning(game)
            matches = Match.objects.filter(game=game, status="upcoming").order_by(
                "postponed", "date"
            )
        return game, [
            m.get_upcoming_content(
                include_team=True if game == "csgo" and not disable_include else False
            )
            for m in matches
        ]
    moose = Team.objects.filter(name="MOUZ").first()
    nxt = Team.objects.filter(name="MOUZ NXT").first()
    mouz = (
        Match.objects.filter(game="csgo", status="upcoming", teams__team__name="MOUZ")
        .order_by("postponed", "date")
        .first()
    )
    mouz_nxt = (
        Match.objects.filter(
            game="csgo", status="upcoming", teams__team__name="MOUZ NXT"
        )
        .order_by("postponed", "date")
        .first()
    )
    mouz_valo = (
        Match.objects.filter(game="valo", status="upcoming", teams__team__name="MOUZ")
        .order_by("postponed", "date")
        .first()
    )
    mouz_dota = (
        Match.objects.filter(game="dota", status="upcoming", teams__team__name="MOUZ")
        .order_by("postponed", "date")
        .first()
    )
    if mouz:
        mouz = mouz.get_upcoming_content()
    else:
        if moose.no_match_msg:
            mouz = moose.no_match_msg
        else:
            mouz = "no upcoming match"
    if mouz_nxt:
        mouz_nxt = mouz_nxt.get_upcoming_content()
    else:
        if nxt.no_match_msg:
            mouz_nxt = nxt.no_match_msg
        else:
            mouz_nxt = "no upcoming match"
    if mouz_valo:
        mouz_valo = mouz_valo.get_upcoming_content()
    else:
        mouz_valo = "no upcoming match"
    if mouz_dota:
        mouz_dota = mouz_dota.get_upcoming_content()
    else:
        mouz_dota = "no upcoming match"
    extra_matches = ExtraMatch.objects.filter(
        Q(enabled=True, ending_at__isnull=True)
        | Q(enabled=True, ending_at__gte=timezone.now())
    ).order_by("starting_at")
    return None, (mouz, mouz_nxt, mouz_valo, mouz_dota, list(extra_matches))


@sync_to_async
def get_results(game=None, team=None):
    if game:
        disable_include = False
        if game in ["csgo", "cs", "cs2"]:
            game = "csgo"
        elif game in ["valo", "valorant", "val", "vlr"]:
            game = "valo"
        elif game in ["dota", "dota2"]:
            game = "dota"
        if team and team in ["nxt", "NXT"]:
            matches = Match.objects.filter(
                game="csgo", status="finished", teams__team__name="MOUZ NXT"
            ).order_by("-date")[:10]
            game = "cs_nxt"
            disable_include = True
        elif team and team == "main":
            matches = Match.objects.filter(
                game="csgo", status="finished", teams__team__name="MOUZ"
            ).order_by("-date")[:10]
            game = "cs_main"
            disable_include = True
        else:
            matches = Match.objects.filter(game=game, status="finished").order_by(
                "-date"
            )[:10]
        return game, [
            m.get_result_content(
                include_team=True if game == "csgo" and not disable_include else False
            )
            for m in matches
        ]
    mouz = Match.objects.filter(
        game="csgo", status="finished", teams__team__name="MOUZ"
    ).order_by("-date")[:5]
    mouz_nxt = Match.objects.filter(
        game="csgo", status="finished", teams__team__name="MOUZ NXT"
    ).order_by("-date")[:5]
    mouz_valo = Match.objects.filter(
        game="valo", status="finished", teams__team__name="MOUZ"
    ).order_by("-date")[:5]
    mouz_dota = Match.objects.filter(
        game="dota", status="finished", teams__team__name="MOUZ"
    ).order_by("-date")[:5]
    if mouz:
        mouz = [m.get_result_content() for m in mouz]
    if mouz_nxt:
        mouz_nxt = [m.get_result_content() for m in mouz_nxt]
    if mouz_valo:
        mouz_valo = [m.get_result_content() for m in mouz_valo]
    if mouz_dota:
        mouz_dota = [m.get_result_content() for m in mouz_dota]
    return None, (mouz, mouz_nxt, mouz_valo, mouz_dota)


async def create_upcoming_embed(game, team):
    embed = discord.Embed(
        title="Upcoming matches",
        color=0xFF0000,
    )
    embed.set_author(
        name="vamouz.de",
        url="https://vamouz.de",
        icon_url="https://vamouz.de/static/home/mouz_small.png",
    )
    embed.set_thumbnail(url="https://vamouz.de/static/home/mouzz.png")
    if game:
        game, matches = await get_upcoming(game=game, team=team)
        logger.warning(matches)
        if game in ["sc2", "sc", "starcraft2", "starcraft"]:
            game = "StarCraft II"
        if game in ["csgo", "cs"]:
            game = "CS2"
        elif game in ["valo", "valorant", "val", "vlr"]:
            game = "VALORANT"
        elif game in ["dota", "dota2"]:
            game = "Dota"
        if team and team in ["nxt", "NXT"]:
            game = game + " NXT"
        elif team and team in ["main"]:
            game = game + " main"
        embed.title = embed.title + " - " + game
        if len(matches) == 0:
            embed.description = "no upcoming matches"
        else:
            embed.description = "\n".join(matches)

    else:
        game, matches = await get_upcoming()
        if not matches[0] == "no upcoming match":
            embed.add_field(name="**CS2**", value=matches[0], inline=False)
        if not matches[1] == "no upcoming match":
            embed.add_field(name="**CS2 NXT**", value=matches[1], inline=False)
        if not matches[2] == "no upcoming match":
            embed.add_field(name="**VALORANT**", value=matches[2], inline=False)
        if not matches[3] == "no upcoming match":
            embed.add_field(name="**Dota**", value=matches[3], inline=False)
        skip = []
        for em in matches[4]:
            if em.game in skip:
                continue
            skip.append(em.game)
            embed.add_field(
                name="**{}**".format(em.title),
                value=em.get_upcoming_content(),
                inline=False,
            )
    return embed


async def upcoming(message, redis):
    pattern = re.compile(
        r"!(upcoming|matches)(\s{1}(?P<game>csgo|cs|lol$|LoL$|sc2$|sc$|starcraft$|starcraft2$|league$|vlr$|val$|valorant$|valo|dota$|dota2$|nxt|main$)(\s{1}(?P<team>main$|NXT$|nxt$))?)?"
    )
    match = pattern.search(message.content)
    match_dict = match.groupdict()
    game = match_dict.get("game")
    team = match_dict.get("team")
    if game in ["nxt", "main"]:
        team = game
        game = "cs"
    embed = await create_upcoming_embed(game, team)
    if len(embed.fields) == 0 and not game:
        embed.description = "No upcoming matches :("
    await message.channel.send(embed=embed)


async def create_results_embed(game, team):
    embed = discord.Embed(
        title="Recent results",
        color=0xFF0000,
    )
    embed.set_author(
        name="vamouz.de",
        url="https://vamouz.de",
        icon_url="https://vamouz.de/static/home/mouz_small.png",
    )
    embed.set_thumbnail(url="https://vamouz.de/static/home/mouzz.png")
    if game:
        game_short, matches = await get_results(game=game, team=team)
        if game in ["csgo", "cs"]:
            game = "CS2"
            if team and team in ["nxt", "NXT"]:
                game = game + " NXT"
            elif team and team in ["main"]:
                game = game + " main"
        elif game in ["valo", "valorant", "val", "vlr"]:
            game = "VALORANT"
        elif game in ["dota", "dota2"]:
            game = "Dota"
        embed.title = embed.title + " - " + game
        if not matches:
            embed.description = "no results"
        else:
            embed.description = "\n".join(matches)
        embed.description = (
            embed.description
            + "\n\n[**more**](https://vamouz.de/results/?game={})".format(game_short)
        )
    else:
        game, matches = await get_results()
        embed.add_field(
            name="CS2",
            value=""
            + "\n".join(matches[0])
            + "\n[**more**](https://vamouz.de/results/?game=cs_main)",
            inline=False,
        )
        embed.add_field(
            name="CS2 NXT",
            value=""
            + "\n".join(matches[1])
            + "\n[**more**](https://vamouz.de/results/?game=cs_nxt)",
            inline=False,
        )
        embed.add_field(
            name="VALORANT",
            value=""
            + "\n".join(matches[2])
            + "\n[**more**](https://vamouz.de/results/?game=valo)",
            inline=False,
        )
        embed.add_field(
            name="Dota",
            value=""
            + "\n".join(matches[3])
            + "\n[**more**](https://vamouz.de/results/?game=dota)",
            inline=False,
        )
    return embed


async def results(message, redis):
    pattern = re.compile(
        r"!results(\s{1}(?P<game>csgo|cs|lol$|LoL$|sc2$|sc$|starcraft$|starcraft2$|league$|vlr$|val$|valorant$|dota$|dota2$|valo|nxt|main$)(\s{1}(?P<team>main$|NXT$|nxt$))?)?"
    )
    match = pattern.search(message.content)
    match_dict = match.groupdict()
    game = match_dict.get("game")
    team = match_dict.get("team")
    if game in ["nxt", "main"]:
        team = game
        game = "cs"
    embed = await create_results_embed(game, team)
    await message.channel.send(embed=embed)


MOD_PLUS_ROLE_IDS = [
    653630204140322826,  # mod
    441541593099403264,  # admin
    554663341054885888,  # founder
    877287261354471494,  # gods
]

CLEARABLE_CHANS = [
    791310931060981800,  # suggestions
    884466855190872105,  # looking-to-play
    995747417263702016,
]


async def clear_chan(message, redis):
    if not message.channel.id in CLEARABLE_CHANS:
        return
    roles = [role.id for role in message.author.roles]
    if not list(set(roles).intersection(MOD_PLUS_ROLE_IDS)):
        return
    msges = []
    async for msg in message.channel.history():
        if not msg.pinned:
            msges.append(msg)
    try:
        await message.channel.delete_messages(msges)
    except:
        pass
    async for msg in message.channel.history():
        if not msg.pinned:
            try:
                await msg.delete()
            except discord.DiscordException:
                continue
    await message.channel.send(
        content="xertioN just came in and cleaned up (this msg will be deleted after 30 seconds)",
        delete_after=30,
    )
    logger.warning(
        "xertioNpeek: {} cleared {}".format(message.author.name, message.channel.name)
    )


@sync_to_async
def add_warning(server, user_id, warned_by, reason):
    server = LevelServer.objects.filter(discord_id=server).first()
    if not server:
        return None
    user = server.levelserveruser_set.filter(discord_id=user_id).first()
    if not user:
        user = LevelServerUser()
        user.server = server
        user.discord_id = user_id
        user.save()
    warned_by = server.levelserveruser_set.filter(discord_id=warned_by).first()
    if not warned_by:
        return None
    w = Warning()
    w.user = user
    w.warned_by = warned_by
    w.message = reason
    w.save()
    return user


@sync_to_async
def get_infractions(server, user):
    server = LevelServer.objects.filter(discord_id=server).first()
    user = server.levelserveruser_set.filter(discord_id=user).first()
    if not user:
        return None, None, None, None, None
    infractions = user.infractions.all()
    total = infractions.count()
    now = timezone.now()
    odayago = now - timezone.timedelta(days=1)
    sdaysago = now - timezone.timedelta(days=7)
    past1 = infractions.filter(date__gt=odayago).count()
    past7 = infractions.filter(date__gt=sdaysago).count()
    recent = list(infractions.order_by("-date")[:3])
    return user, recent, total, past1, past7


async def infractions(message, r):
    roles = [role.id for role in message.author.roles]
    if not list(set(roles).intersection(MOD_PLUS_ROLE_IDS)):
        return
    pattern = re.compile(r"!infractions\s(<\@!?(?P<user_id>\d+)\>|(?P<user_id1>\d+))")
    match = pattern.search(message.content)
    match_dict = match.groupdict()
    if match_dict.get("user_id"):
        user_id = match_dict.get("user_id")
    elif match_dict.get("user_id1"):
        user_id = match_dict.get("user_id1")
    else:
        return
    r, recent, total, past1, past7 = await get_infractions(message.guild.id, user_id)
    if not r:
        return
    embed = discord.Embed(
        color=0xFF0000,
    )
    embed.set_author(name="{}'s infractions ".format(r.name), icon_url=r.avatar)
    embed.add_field(
        name="Last 24 Hours", value="{} infractions".format(past1), inline=True
    )
    embed.add_field(
        name="Last 7 days", value="{} infractions".format(past7), inline=True
    )
    embed.add_field(name="Total", value="{} infractions".format(total), inline=True)
    if recent:
        embed.add_field(
            name="Recent infractions",
            value=""
            + "\n".join(
                ["**{}** - {}".format(x.message, naturaltime(x.date)) for x in recent]
            )
            + "\n[\[manage {}'s infractions\]](https://vamouz.de/vamouz/ropzpeek/warning/?q={})".format(
                r.name, r.discord_id
            ),
        )
    else:
        embed.add_field(
            name="\u200b",
            value="\n[\[manage {}'s infractions\]](https://vamouz.de/vamouz/ropzpeek/warning/?q={})".format(
                r.name, r.discord_id
            ),
        )

    await message.channel.send(embed=embed)


async def warn(message, r):
    roles = [role.id for role in message.author.roles]
    if not list(set(roles).intersection(MOD_PLUS_ROLE_IDS)):
        await message.channel.send("nt noob")
        return
    pattern = re.compile(
        r"!warn\s(<\@!?(?P<user_id>\d+)\>|(?P<user_id1>\d+))(\s(?P<reason>[.\s\S]*))?"
    )
    match = pattern.search(message.content)
    if not match:
        logger.warning(message.content)
    match_dict = match.groupdict()
    logger.warning(match_dict)
    if match_dict.get("user_id"):
        user_id = match_dict.get("user_id")
    elif match_dict.get("user_id1"):
        user_id = match_dict.get("user_id1")
    else:
        return
    logger.warning(message.author.id)
    logger.warning(user_id)
    if int(message.author.id) == int(user_id):
        return
    reason = ""
    if match_dict.get("reason"):
        reason = match_dict.get("reason")
    r = await add_warning(message.guild.id, user_id, message.author.id, reason)
    if not r:
        logger.warning("warning failed")
        return
    embed = discord.Embed(
        color=0xFF0000,
    )
    if not r.name:
        r.name = str(r.discord_id)
    if not r.avatar:
        embed.set_author(name="{} has been warned".format(r.name))
    else:
        embed.set_author(name="{} has been warned".format(r.name), icon_url=r.avatar)
    embed.add_field(name="**Reason**", value=reason)
    await message.channel.send(embed=embed)

    channel = message.guild.get_channel(906254769780314192)
    if channel:
        embed = discord.Embed(
            color=0xFF0000,
        )
        if not r.avatar:
            embed.set_author(name="[WARN] {}".format(r.name))
        else:
            embed.set_author(name="[WARN] {}".format(r.name), icon_url=r.avatar)
        embed.add_field(name="**User**", value="<@!{}>".format(user_id), inline=True)
        embed.add_field(
            name="**Moderator**", value="<@!{}>".format(message.author.id), inline=True
        )
        embed.add_field(
            name="**Channel**", value="<#{}>".format(message.channel.id), inline=True
        )
        embed.add_field(
            name="**Reason**",
            value=reason,
        )
        await channel.send(embed=embed)
    logger.warning("warned {}".format(user_id))


async def help(message, r):
    embed = discord.Embed(
        title="Command list",
        color=0xFF0000,
    )
    embed.set_author(
        name="vamouz.de",
        url="https://vamouz.de",
        icon_url="https://vamouz.de/static/home/mouz_small.png",
    )
    embed.set_thumbnail(url="https://vamouz.de/static/home/mouz_small.png")
    embed.description = "\n".join(
        [
            "**!upcoming** (cs|lol|vlr) (main|nxt)\nupcoming MOUZ matches - you can pass a specific game as argument",
            "**!results** (cs|lol|vlr) (main|nxt)\nupcoming MOUZ matches - you can pass a specific game as argument",
            "**!rank**\nshows the current rank and level in the ",
        ]
    )
    await message.channel.send(embed=embed)


QUOTE_CHANS = [
    441534100558446598,
    497900798638817280,
    494476435595984904,
    489874067470680069,
]


def quotefilter(message):
    if message.author.bot:
        return False
    if message.author.discriminator == "0000":
        return False
    if not message.content:
        return False
    if message.embeds or message.attachments:
        return False
    if not len(message.content.split(" ")) > 2:
        return False
    return True


@sync_to_async
def store_quote_in_db(quote):
    q = Quote()
    q.date = timezone.now()
    q.data = {
        "server_id": quote.guild.id,
        "channel_id": quote.channel.id,
        "message_id": quote.id,
        "author_id": quote.author.id,
        "author_name": quote.author.name,
        "message_date": quote.created_at.timestamp(),
        "message_content": quote.content,
    }
    q.save()


@sync_to_async
def get_prev_quote_ids():
    return list(
        Quote.objects.annotate(
            msg_id=KeyTextTransform("message_id", "data")
        ).values_list("msg_id", flat=True)
    )


async def quote(message, r):
    ber = pytz.timezone("Europe/Berlin")
    now = timezone.now().astimezone(ber)
    now_str = "{}_{}".format(message.guild.id, now.strftime("%m_%d_%Y"))
    msg_id = await r.get(now_str)
    quote = None
    if not msg_id:
        prev_quote_ids = await get_prev_quote_ids()
        created = message.channel.guild.created_at
        delta = now - created
        int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
        quote = None
        while not quote:
            random_second = random.randrange(int_delta)
            random_date = created + timezone.timedelta(seconds=random_second)
            options = []
            channel = message.guild.get_channel(random.choice(QUOTE_CHANS))
            if not channel:
                channel = message.channel
            async for m in channel.history(limit=50, around=random_date):
                if quotefilter(m) and str(m.id) not in prev_quote_ids:
                    options.append(m)
            try:
                quote = random.choice(options)
            except:
                continue
            for q in options:
                logger.warning(q)
                logger.warning(q.author.name)
            await r.set(now_str, "{}_{}".format(quote.channel.id, quote.id))
            if message.channel.guild.id == 441534100558446594:
                await store_quote_in_db(quote)
    if not quote:
        channel_id = int(msg_id.decode().split("_")[0])
        quote_id = int(msg_id.decode().split("_")[1])
        logger.warning(quote_id)
        quote = await message.guild.get_channel(channel_id).fetch_message(quote_id)
    timestamp = int(quote.created_at.timestamp())
    date_string = "<t:{}:R>".format(timestamp)
    embed = discord.Embed(
        title="Quote of the day",
        color=0xFF0000,
    )
    embed.description = "{}\n\n{}".format(
        quote.content,
        "{} - {} - [Original message]({}) - [Archive]({})".format(
            quote.author.name, date_string, quote.jump_url, "https://vamouz.de/quotes/"
        ),
    )
    embed.set_author(
        name="vamouz.de",
        url="https://vamouz.de",
        icon_url="https://vamouz.de/static/home/mouz_small.png",
    )
    embed.set_thumbnail(url="https://vamouz.de/static/home/mouzz.png")
    await message.channel.send(embed=embed)


async def level_handler(r, message, delete=False):
    if message.author.bot:
        return
    if delete:
        await r.hincrby(
            "msges", "{}_{}".format(message.channel.guild.id, message.author.id), -1
        )
        return
    ts = int(round(message.created_at.timestamp()))
    await r.hincrby(
        "msges", "{}_{}".format(message.channel.guild.id, message.author.id), 1
    )
    await r.hset(
        "xp",
        "{}_{}_{}".format(message.channel.guild.id, message.author.id, ts - (ts % 30)),
        1,
    )
    await r.hset(
        "user_name",
        "{}_{}".format(message.channel.guild.id, message.author.id),
        message.author.display_name,
    )
    if message.author.avatar:
        await r.hset(
            "user_avatars",
            "{}_{}".format(message.channel.guild.id, message.author.id),
            str(message.author.avatar.replace(format="png", size=256)),
        )


async def rank(message, redis):
    pattern = re.compile(r"!rank\s{0,10}(?P<user>\<\@!?(?P<user_id>\d+)\>)?")
    match = pattern.search(message.content)
    match_dict = match.groupdict()
    user_id = message.author.id
    if match_dict.get("user_id"):
        user_id = match_dict.get("user_id")
    data = await redis.get("{}_{}".format(message.channel.guild.id, user_id))
    if not data:
        await message.channel.send(
            content="Oopsie, something went wrong. `!rank [@user]`"
        )
        return
    data = json.loads(data)
    embed = discord.Embed(
        title="{} - #{}".format(data.get("name"), data.get("rank")),
        color=0xFF0000,
    )
    embed.set_author(
        name="vamouz.de",
        url="https://vamouz.de",
        icon_url="https://vamouz.de/static/home/mouz_small.png",
    )
    embed.set_thumbnail(url=data.get("avatar"))
    embed.add_field(
        name="Level",
        value="{} - {}".format(data.get("level"), data.get("level_name")),
        inline=True,
    )
    embed.add_field(
        name="XP",
        value="**{}**/{}".format(
            pretty_number(data.get("max_xp")),
            pretty_number(data.get("level_max_xp") + 1),
        ),
        inline=True,
    )
    embed.add_field(
        name="Messages", value=pretty_number(data.get("msges")), inline=True
    )
    if message.channel.guild.id == 441534100558446594:
        embed.add_field(name="Ranking", value="https://vamouz.de/ranking")
    else:
        embed.add_field(
            name="Ranking",
            value="https://vamouz.de/ranking/{}".format(message.channel.guild.id),
        )
    await message.channel.send(embed=embed)
