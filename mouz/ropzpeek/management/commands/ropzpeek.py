import aioredis
import asyncio
import datetime
import discord
import json
import logging
import random
import re
import sys
import traceback
from . import ropzpeek_handlers, ropzpeek_tasks
from asgiref.sync import sync_to_async
from discord import app_commands
from discord.ext.tasks import loop
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.models import F
from django.utils import timezone
from home.models import Match
from ropzpeek.models import (
    Command as DiscordCommand, LevelServer, LevelServerLevel, LevelServerRole,
    LevelServerUser, LevelSnapshot,
)
from typing import List, Optional

MY_GUILD = discord.Object(id=864559179775082506)  # replace with your guild id

MOD_PLUS_ROLE_IDS = [
    653630204140322826,  # mod
    441541593099403264,  # admin
    554663341054885888,  # founder
    877287261354471494,  # gods
]


class MyClient(discord.Client):
    def __init__(self, *, intents: discord.Intents):
        super().__init__(intents=intents)
        self.tree = app_commands.CommandTree(self)

    async def setup_hook(self):
        self.tree.copy_global_to(guild=MY_GUILD)
        await self.tree.sync()
        self.loop.create_task(check_for_clear())
        self.loop.create_task(check_for_role_updates())
        self.loop.create_task(sync_commands())
        self.loop.create_task(ropzpeek_tasks.sync_levels())
        self.loop.create_task(ropzpeek_tasks.update_level_cache())
        self.loop.create_task(check_for_msges())


intents = discord.Intents.default()
intents.members = True
intents.message_content = True
client = MyClient(intents=intents)


logger = logging.getLogger(__name__)

TOKEN = settings.DISCORD_TOKEN

AUTO_CLEAR_CHANS = [
    {
        "id": 884466855190872105,  # channel id
        "threshold": 60
        * 4,  # how old the latest msg must be to wipe the channel (minutes)
    }
]
IMAGE_ONLY_CHANS = [702088411967651880, 547765207569661955]

commands = {}

cmd_mappings = {
    "!upcoming": ropzpeek_handlers.upcoming,
    "!matches": ropzpeek_handlers.upcoming,
    "!results": ropzpeek_handlers.results,
    "!clear_chan": ropzpeek_handlers.clear_chan,
    "!rank": ropzpeek_handlers.rank,
    "!warn": ropzpeek_handlers.warn,
    "!infractions": ropzpeek_handlers.infractions,
    "!quote": ropzpeek_handlers.quote,
    "!help": ropzpeek_handlers.help,
}


FORBIDDEN_PINGS = [
    "189481233011441664",  # acoR
    "176691398848937984",  # ropz
    "194548848620732416",  # Bymas
    "610545855174541328",  # frozen
    "393895108858085376",  # szejn
    "634450424090591233",  # siuhy
    "300306629642027010",  # JDC
    "229637707830263808",  # xertioN
    "678639158952067072",  # torzsi
    "374142265905643522",  # al0rante
    "378133050325991425",  # EnTe
    "263165609443852289",  # HeroMarine
    "602843272246722571",  # memset
    "162325867484282883",  # ProblemX
    "508681191688372225",  # Rueven
    "385493908920074260",  # stompy
    "201679489124532224",  # WaN
]


HEY_ROPZPEEK = [
    (653630204140322826, 1),
    (441541593099403264, 1),
    (556905064162131969, 1),
    (554663341054885888, 1),
    (441557567987122178, 2),
    (441563529393340428, 3),
]

"""Gets the likeihood frozenpeek replies friendly for a user
:param user: The user to calculate the likelihood for
:type user: discord.Member
:returns: the likelihood 
:rtype: int
"""


async def get_ropzpeek_likelihood(user):
    for r in HEY_ROPZPEEK:
        for ur in user.roles:
            if r[0] == ur.id:
                return r[1]
    return 999


"""Clears the channels defined in AUTO_CLEAR_CHANS
"""


async def check_for_clear():
    await client.wait_until_ready()
    while True:
        for c in AUTO_CLEAR_CHANS:
            channel = client.get_channel(c.get("id"))
            if not channel:
                continue
            msges = []
            async for msg in channel.history():
                if not msg.pinned:
                    msges.append(msg)
            threshold = discord.utils.utcnow() - datetime.timedelta(
                minutes=c.get("threshold", sys.maxsize)
            )
            if msges and msges[0].created_at < threshold:
                logger.warning("xertioNpeek: cleared {}".format(channel.name))
                await channel.delete_messages(msges)
                async for msg in channel.history():
                    if not msg.pinned:
                        try:
                            await msg.delete()
                        except discord.DiscordException as e:
                            logger.warning(
                                "xertioNpeek: clear error: ".format(msg.content)
                            )
                            logger.warning(e)
                            continue
                await channel.send(
                    content="xertioN just came in and cleaned up (this msg will be deleted after 30 seconds)",
                    delete_after=30,
                )
        await asyncio.sleep(60 * 5)


@sync_to_async
def get_server_users():
    ls = LevelServer.objects.get(discord_id=864559179775082506)
    r = (
        ls.levelserveruser_set.all()
        .annotate(level_role_id=F("current_level__role__discord_id"))
        .values()
    )
    roles = ls.levelserverrole_set.all().values()
    return list(r), list(roles)


@sync_to_async
def create_user(user):
    ls = LevelServer.objects.get(discord_id=864559179775082506)
    lsu = ls.levelserveruser_set.filter(discord_id=user.id).first()
    if not lsu:
        lsu = LevelServerUser()
        lsu.discord_id = user.id
        lsu.server = ls
        lsu.name = user.nick
        lsu.avatar = user.avatar.replace(format="png", size=256)
        lsu.save()


async def check_for_role_updates():
    await client.wait_until_ready()
    logger.warning("check_for_role_updates")
    users, roles = await get_server_users()
    await asyncio.sleep(60 * 5)


@sync_to_async
def get_commands():
    return list(DiscordCommand.objects.filter(active=True).values())


"""Periodically updates the defined commands from database 
"""


async def sync_commands():
    logger = logging.getLogger(__name__)
    while True:
        global commands
        commands = {}
        cmds = await get_commands()
        for c in cmds:
            commands[c.get("command")] = c
        await asyncio.sleep(20)


async def check_for_msges():
    redis = aioredis.from_url("redis://redis:6379/9")
    while True:
        msg = await redis.lpop("fpeek_msges")
        while msg:
            logger.warning(msg)
            data = json.loads(msg)
            channel = client.get_channel(int(data.get("channel", -1)))
            if channel:
                await channel.send(content=data.get("message"))
            msg = await redis.lpop("fpeek_msges")
        await asyncio.sleep(1)


class Command(BaseCommand):
    def handle(self, *args, **options):
        @client.event
        async def on_ready():
            logger.info("start")
            self.redis = aioredis.from_url("redis://redis:6379/9")
            channel = client.get_channel(441534100558446598)
            # await channel.send(content="Hello")

        @client.event
        async def on_message_delete(message):
            await ropzpeek_handlers.level_handler(self.redis, message, delete=True)

        @client.event
        async def on_member_join(member):
            if not member.bot:
                await create_user(member)

        @client.event
        async def on_message(message):
            await client.wait_until_ready()

            if message.author == client.user:
                return

            if not isinstance(message.author, discord.Member):
                return

            if (
                message.content.lower() == "hey xertionpeek"
                or message.content.lower() == "hi xertionpeek"
            ):
                fac = await get_ropzpeek_likelihood(message.author)
                if random.randint(0, fac) == 0:
                    await message.channel.send(
                        content="Hey {}, big fan".format(message.author.name)
                    )
                else:
                    await message.channel.send(
                        content=random.choice(
                            [
                                "Don't talk to me",
                                "I'm not in the mood rn",
                                "The person you have called is temporarily not available",
                                "I don't talk to people with less than 3.1k elo",
                                "Let me rot in peace",
                                "Step up your game, maybe next time",
                            ]
                        )
                    )
                return

            if message.content == "!hltv":
                embed = discord.Embed(url="https://hltv.org", title="https://hltv.org")
                embed.set_image(
                    url="https://cdn-longterm.mee6.xyz/plugins/commands/images/441534100558446594/cc25cbc4d03d682a807dd0e2590b117a1ae2559cef9836457edab2208c083b35.png"
                )
                await message.channel.send(embed=embed)
            elif message.content == "!vlr":
                embed = discord.Embed(url="https://vlr.gg", title="https://vlr.gg")
                embed.set_image(
                    url="https://cdn.discordapp.com/attachments/441534100558446598/944012637006610492/VLR.png"
                )
                await message.channel.send(embed=embed)

            # handle player pings
            for p in FORBIDDEN_PINGS:
                if p in message.content:
                    channel = client.get_channel(906254769780314192)
                    await channel.send(
                        "{} (ID: {}) pinged {} (ID: {})".format(
                            message.author.name,
                            message.author.id,
                            message.mentions[0].name,
                            message.mentions[0].id,
                        )
                    )
                    await message.author.send("don't ping players!")

            roles = []
            roles = [role.id for role in message.author.roles]

            # handle potential scams and non-mouz discord invite links
            if not list(set(roles).intersection(MOD_PLUS_ROLE_IDS)):
                if (
                    "free" in message.content.lower()
                    and "nitro" in message.content.lower()
                    and "http" in message.content.lower()
                ):
                    await message.delete()
                    if message.channel.guild.id == 441534100558446594:
                        channel = client.get_channel(906254769780314192)
                        await channel.send(
                            "{} (ID: {}) sent a potential scam message which got deleted. \n**Message:** \n{}".format(
                                message.author.name,
                                message.author.id,
                                discord.utils.escape_mentions(message.content),
                            )
                        )
                pattern = re.compile(
                    r"(https?://)?(www.)?(discord.(gg|io|me|li)|discordapp.com/invite|discord.com/invite)/((?!mousesports)[^\s/]+?)(?=\b)"
                )
                match = pattern.search(message.content)
                if match and message.channel.guild.id == 441534100558446594:
                    await message.delete()
                    channel = client.get_channel(906254769780314192)
                    await channel.send(
                        "{} (ID: {}) sent a msg with a discord invite that got deleted. \n**Message:** \n{}".format(
                            message.author.name,
                            message.author.id,
                            discord.utils.escape_mentions(message.content),
                        )
                    )

            # handle messages without attachments in image only chans
            if message.channel.id in IMAGE_ONLY_CHANS:
                if not message.attachments:
                    await message.delete()
                    await message.author.send(
                        content="Hey, #{} is an image-only channel!".format(
                            message.channel.name
                        )
                    )

            # level system
            await ropzpeek_handlers.level_handler(self.redis, message)

            # other handlers for specific commands
            for cmd, handler in cmd_mappings.items():
                if message.content.startswith(cmd) and message.content != "!ranking":
                    await handler(message, self.redis)

            # handle the dynamic commands defined in the django admin
            for cmd, handler in commands.items():
                cmd = commands.get(message.content.lower())
            if cmd:
                channels = cmd.get("channels")
                roles = cmd.get("roles")
                ttl = cmd.get("ttl")
                use_embed = cmd.get("use_embed")
                if channels and message.channel.id not in channels:
                    return
                if roles:
                    user_roles = [role.id for role in message.author.roles]
                    if not list(set(roles).intersection(user_roles)):
                        return
                if use_embed:
                    embed = discord.Embed(
                        title=cmd.get("commmand"),
                        color=0xFF0000,
                    )
                    embed.set_author(
                        name="vamouz.de",
                        url="https://vamouz.de",
                        icon_url="https://vamouz.de/static/home/mouz_small.png",
                    )
                    embed.set_thumbnail(url="https://vamouz.de/static/home/mouzz.png")
                    embed.description = cmd.get("reply")
                    if ttl:
                        await message.channel.send(embed=embed, delete_after=ttl)
                    else:
                        await message.channel.send(embed=embed)
                    return
                if ttl:
                    await message.channel.send(
                        content=cmd.get("reply"), delete_after=ttl
                    )
                else:
                    await message.channel.send(content=cmd.get("reply"))

        async def map_game(game):
            game_chosen, team = None, None
            if game == "CS2":
                game_chosen = "cs"
            elif game == "CS2 Main":
                game_chosen = "cs"
                team = "main"
            elif game == "CS2 NXT":
                game_chosen = "cs"
                team = "nxt"
            elif game == "StarCraft II":
                game_chosen = "sc2"
            elif game == "League of Legends":
                game_chosen = "lol"
            elif game == "VALORANT":
                game_chosen = "vlr"
            elif game == "Dota":
                game_chosen = "dota"
            return team, game_chosen

        @client.tree.command()
        async def upcoming(
            interaction: discord.Interaction, game: Optional[str] = None
        ):
            team, game_chosen = await map_game(game)
            embed = await ropzpeek_handlers.create_upcoming_embed(game_chosen, team)
            await interaction.response.send_message(embed=embed)

        @client.tree.command()
        async def results(interaction: discord.Interaction, game: Optional[str] = None):
            team, game_chosen = await map_game(game)
            embed = await ropzpeek_handlers.create_results_embed(game_chosen, team)
            await interaction.response.send_message(embed=embed)

        @upcoming.autocomplete("game")
        @results.autocomplete("game")
        async def game_autocomplete(
            interaction: discord.Interaction,
            current: str,
        ) -> List[app_commands.Choice[str]]:
            games = [
                "CS2",
                "CS2 Main",
                "CS2 NXT",
                "League of Legends",
                "VALORANT",
                "StarCraft II",
                "Dota",
            ]
            return [
                app_commands.Choice(name=game, value=game)
                for game in games
                if current.lower() in game.lower()
            ]

        client.run(TOKEN)
