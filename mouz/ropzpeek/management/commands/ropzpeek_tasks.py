import aioredis
import asyncio
import logging
import random
from asgiref.sync import sync_to_async
from django.utils import timezone
from ropzpeek.models import (
    LevelServer, LevelServerLevel, LevelServerUser, LevelSnapshot,
)

logger = logging.getLogger(__name__)


@sync_to_async
def apply_levels(data):
    for server_id, v in data.items():
        ls = LevelServer.objects.filter(discord_id=int(server_id)).first()
        if not ls:
            continue
        min_xp = ls.min_xp_reward
        max_xp = ls.max_xp_reward
        for user_id, w in v.items():
            lsu = ls.levelserveruser_set.filter(discord_id=int(user_id)).first()
            if not lsu:
                lsu = LevelServerUser()
                lsu.discord_id = int(user_id)
                lsu.server = ls
            name = w.get("name")
            if name:
                lsu.name = name.decode()
            avatar = w.get("avatar")
            if avatar:
                lsu.avatar = avatar.decode()
            lsu.save()
            logger.warning("update: {} - {}".format(ls.name, lsu.name))
            last_snapshot = lsu.levelsnapshot_set.order_by("-date").first()
            lsnap = LevelSnapshot()
            lsnap.user = lsu
            lsnap.date = timezone.now()
            lsnap.xp_count = 0
            lsnap.msg_count = 0
            if last_snapshot:
                lsnap.msg_count = last_snapshot.msg_count
                lsnap.xp_count = last_snapshot.xp_count
            lsnap.msg_count += w.get("msges", 0)
            for i in range(w.get("xp", 0)):
                lsnap.xp_count += random.randint(min_xp, max_xp)
            lsnap.save()
            lsu.update_level()


async def sync_levels():
    r = aioredis.from_url("redis://redis:6379/9")
    while True:
        xp = await r.hgetall("xp")
        msges = await r.hgetall("msges")
        names = await r.hgetall("user_name")
        avatars = await r.hgetall("user_avatars")
        await r.delete("xp")
        await r.delete("msges")
        await r.delete("user_name")
        await r.delete("user_avatars")
        updates = {}
        for k, v in xp.items():
            key_spl = k.decode().split("_")
            server = key_spl[0]
            user = key_spl[1]
            check = updates.get(server)
            if not check:
                updates[server] = {}
            check = updates.get(server).get(user)
            if not check:
                updates[server][user] = {"xp": 1}
            else:
                updates[server][user]["xp"] = check.get("xp") + 1
        for k, v in msges.items():
            key_spl = k.decode().split("_")
            server = key_spl[0]
            user = key_spl[1]
            check = updates.get(server)
            if not check:
                updates[server] = {}
            check = updates.get(server).get(user)
            if not check:
                updates[server][user] = {"msges": int(v)}
            else:
                updates[server][user]["msges"] = int(v)
        for k, v in names.items():
            key_spl = k.decode().split("_")
            server = key_spl[0]
            user = key_spl[1]
            check = updates.get(server)
            if not check:
                updates[server] = {}
            check = updates.get(server).get(user)
            if not check:
                updates[server][user] = {"name": v}
            else:
                updates[server][user]["name"] = v
        for k, v in avatars.items():
            key_spl = k.decode().split("_")
            server = key_spl[0]
            user = key_spl[1]
            check = updates.get(server)
            if not check:
                updates[server] = {}
            check = updates.get(server).get(user)
            if not check:
                updates[server][user] = {"avatar": v}
            else:
                updates[server][user]["avatar"] = v
        await apply_levels(updates)
        await asyncio.sleep(600)


@sync_to_async
def update_level_cache_db():
    for s in LevelServer.objects.all():
        s.store_ranking_in_cache()


async def update_level_cache():
    while True:
        await update_level_cache_db()
        await asyncio.sleep(300)
