# Generated by Django 3.1 on 2021-09-05 17:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("top20", "0010_auto_20210905_1901"),
    ]

    operations = [
        migrations.AddField(
            model_name="compchoice",
            name="diff",
            field=models.IntegerField(default=-1),
        ),
    ]
