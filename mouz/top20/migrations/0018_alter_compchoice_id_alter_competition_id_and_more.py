# Generated by Django 4.2 on 2025-03-10 19:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('top20', '0017_compsubmit_disqualified'),
    ]

    operations = [
        migrations.AlterField(
            model_name='compchoice',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='competition',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='compsubmit',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
