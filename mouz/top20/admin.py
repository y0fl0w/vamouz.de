from . import models
from django.contrib import admin
from django.forms import ModelForm


class CompetitionAdminForm(admin.ModelAdmin):
    search_fields = ["name"]


class CompetitionChoiceInline(admin.TabularInline):
    model = models.CompChoice
    extra = 0
    show_change_link = True


class CompetitionSubmitAdminForm(admin.ModelAdmin):
    inlines = [CompetitionChoiceInline]
    search_fields = ["competition__name", "user__username"]


class CompetitionChoiceAdminForm(admin.ModelAdmin):
    search_fields = ["csubmit__competition__name", "csubmit__user__username"]


admin.site.register(models.Competition, CompetitionAdminForm)
admin.site.register(models.CompSubmit, CompetitionSubmitAdminForm)
admin.site.register(models.CompChoice, CompetitionChoiceAdminForm)
