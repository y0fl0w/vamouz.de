from django import template

register = template.Library()


@register.filter()
def correctPick(comp, index):
    master = comp.compsubmit_set.filter(
        user__username="master", user__is_staff=True
    ).first()
    if master:
        correctPick = master.compchoice_set.filter(index=index).first()
        if correctPick:
            return correctPick.val
    return ""


@register.filter()
def get_avatar_url(cs):
    lsu = cs.user.levelserveruser_set.first()
    if lsu and lsu.avatar:
        return lsu.avatar
    return "https://vamouz.de/static/home/mouz_small.png"
