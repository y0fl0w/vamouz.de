from . import models
from django.contrib.auth import get_user_model
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ["username"]


class CompChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CompChoice
        fields = ["index", "val"]


class CompSubmitSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    compchoice_set = CompChoiceSerializer(many=True)

    class Meta:
        model = models.CompSubmit
        fields = ["user", "submitted_at", "updated_at", "compchoice_set"]
        depth = 1
