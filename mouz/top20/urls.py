from . import views
from django.urls import path
from django.views.generic import TemplateView

app_name = "top20"
urlpatterns = [
    path("<slug:slug>/", views.CompetitionView.as_view(), name="detail"),
    path("<slug:slug>/submit/", views.CompetitionSubmitView.as_view(), name="submit"),
    path(
        "<slug:comp>/<int:pk>-<str:username>",
        views.CompetitionSubmitDetailView.as_view(),
        name="submit-detail",
    ),
]
