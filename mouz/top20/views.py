import logging
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count, F, Q, Sum, Window
from django.db.models.functions import Coalesce, DenseRank
from django.forms import HiddenInput, IntegerField, TextInput, formset_factory
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views import generic
from django.views.generic.edit import FormMixin, FormView
from top20 import forms, models

logger = logging.getLogger(__name__)


class CompetitionView(generic.DetailView):
    template_name = "top20/competition.html"
    model = models.Competition

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["participants"] = (
            self.object.compsubmit_set.exclude(
                user__username="master", user__is_staff=True
            )
            .annotate(
                sum_points=Coalesce(Sum("compchoice__points"), 0),
                correct_picks=Count(
                    "compchoice__id", filter=Q(compchoice__correct=True)
                ),
                one_off_picks=Count("compchoice__id", filter=Q(compchoice__diff=1)),
                two_off_picks=Count("compchoice__id", filter=Q(compchoice__diff=2)),
                in_picks=Count("compchoice__id", filter=Q(compchoice__points=1)),
            )
            .annotate(
                dense_rank=Window(
                    expression=DenseRank(),
                    order_by=[
                        F(
                            "sum_points",
                        ).desc(),
                        F(
                            "correct_picks",
                        ).desc(),
                        F(
                            "one_off_picks",
                        ).desc(),
                        F(
                            "two_off_picks",
                        ).desc(),
                        F(
                            "in_picks",
                        ).desc(),
                    ],
                )
            )
            .order_by("dense_rank")
        )

        context["active"] = self.object.is_active()
        context["finished"] = self.object.is_finished()
        master = self.object.compsubmit_set.filter(
            user__username="master", user__is_staff=True
        ).first()
        if master:
            context["rn"] = master.compchoice_set.filter(
                csubmit__competition=self.object
            ).count()
        else:
            context["rn"] = 0
        context["total"] = self.object.val_amount
        if self.request.user.is_authenticated:
            submit = self.object.compsubmit_set.filter(user=self.request.user).first()
            context["submit"] = submit
            user = self.object.compsubmit_set.filter(user=self.request.user).first()
            if user:
                context["user_rn"] = (
                    user.compchoice_set.filter(csubmit__competition=self.object)
                    .exclude(val="")
                    .count()
                )
            else:
                context["user_rn"] = 0
        return context


class CompetitionSubmitDetailView(generic.DetailView):
    template_name = "top20/competition_submit_detail.html"
    model = models.CompSubmit

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["guesses"] = self.object.compchoice_set.order_by("index")
        context["correct"] = self.object.compchoice_set.filter(correct=True).count()
        context["points"] = self.object.compchoice_set.aggregate(
            points=Coalesce(Sum("points"), 0)
        ).get("points", 0)
        context["is_active"] = self.object.competition.is_active()
        master = self.object.competition.compsubmit_set.filter(
            user__username="master", user__is_staff=True
        ).first()
        if master:
            context["rn"] = master.compchoice_set.filter(
                csubmit__competition=self.object.competition
            ).count()
        else:
            context["rn"] = 0
        return context


class CompetitionSubmitView(LoginRequiredMixin, generic.DetailView):
    template_name = "top20/competition_submit.html"
    model = models.Competition

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["active"] = self.object.is_active()
        context["finished"] = self.object.is_finished()
        formset = kwargs.get("form")
        if formset:
            for i, form in enumerate(formset):
                formset[i].fields["name"].label = "#{}".format(str(i + 1))
                logger.warning("added num")
            context["form"] = formset
            for i, form in enumerate(formset):
                logger.warning(form)
                logger.warning(form.fields)
            return context
        if self.request.user.is_authenticated:
            submit = self.object.compsubmit_set.filter(user=self.request.user).first()
            context["submit"] = submit
            formset = formset_factory(forms.CompetitionSubmitValueForm, extra=20)
            formset = formset()
            submit_vals = None
            if submit:
                submit_vals = submit.compchoice_set.all()
            for i, form in enumerate(formset):
                exists = None
                if submit_vals:
                    exists = submit_vals.filter(index=i + 1).first()
                formset[i].fields["name"].label = "#{}".format(str(i + 1))
                if exists:
                    formset[i].fields["name"].widget = TextInput(
                        attrs={
                            "class": "form-control",
                            "disabled": not context["active"],
                            "value": exists.val,
                        }
                    )
                else:
                    formset[i].fields["name"].widget = TextInput(
                        attrs={
                            "class": "form-control",
                            "disabled": not context["active"],
                        }
                    )
                formset[i].fields["pos"].initial = i + 1
            context["form"] = formset
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.is_active():
            return HttpResponseRedirect(
                reverse("top20:detail", kwargs={"slug": self.object.slug})
            )
        submit = self.object.compsubmit_set.filter(user=self.request.user).first()
        submit_vals = None
        now = timezone.now()
        if submit:
            submit_vals = submit.compchoice_set.all()
            submit.updated_at = now
        else:
            submit = models.CompSubmit()
            submit.user = request.user
            submit.competition = self.object
            submit.submitted_at = now
            submit.updated_at = now
        submit.save()
        formset = formset_factory(
            forms.CompetitionSubmitValueForm, formset=forms.CompetitionFormSet, extra=20
        )
        form = formset(request.POST)
        logger.warning(request.POST)
        if form.is_valid():
            logger.warning("form valid")
            for f in form:
                name = f.cleaned_data.get("name")
                pos = f.cleaned_data.get("pos")
                exists = None
                if submit_vals:
                    exists = submit_vals.filter(index=pos).first()
                if exists and exists.val != name:
                    if name != "":
                        exists.val = name
                        exists.save()
                    else:
                        exists.delete()
                elif not exists and name != "":
                    exists = models.CompChoice()
                    exists.csubmit = submit
                    exists.index = pos
                    exists.val = name
                    exists.save()
            return HttpResponseRedirect(
                reverse("top20:detail", kwargs={"slug": self.object.slug})
            )
        else:
            logger.warning("form invalid")
            return self.render_to_response(self.get_context_data(form=form))
