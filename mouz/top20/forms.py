import logging
from django import forms
from django.core.exceptions import ValidationError
from django.forms import BaseFormSet, formset_factory

logger = logging.getLogger(__name__)


class CompetitionSubmitValueForm(forms.Form):
    name = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
    )
    pos = forms.IntegerField(
        label="",
        widget=forms.HiddenInput(),
    )

    def clean(self):
        logger.warning("f clean")
        cleaned_data = super().clean()


class CompetitionFormSet(BaseFormSet):
    def clean(self):
        logger.warning("fs clean")
        if any(self.errors):
            return
        names = []
        for form in self.forms:
            if self.can_delete and self._should_delete_form(form):
                continue
            name = form.cleaned_data.get("name")
            if name == "":
                continue
            if name in names:
                raise ValidationError("Player names in a set must have distinct names")
            names.append(name)
