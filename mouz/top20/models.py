import json
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import Count, F
from django.utils import timezone
from django.utils.text import slugify
from home.models import BaseEvent
from markdownfield.models import MarkdownField, RenderedMarkdownField
from markdownfield.validators import VALIDATOR_STANDARD


def points_default():
    return {"0": 8, "1": 4, "2": 2, "is_in": 1}


class Competition(BaseEvent):
    val_amount = models.IntegerField(
        default=0, verbose_name="#Slots", help_text="How many slots the competition has"
    )
    extern = models.BooleanField(
        default=False, help_text="Is this a competition hosted on another website?"
    )
    url = models.URLField(
        blank=True, null=True, help_text="URL to the competition if it's external"
    )
    visible = models.BooleanField(
        default=True, help_text="Is the competition visibile to normal users?"
    )
    points = models.JSONField(
        blank=True,
        null=True,
        help_text="""
            maps the deviation of a guess to points, e.g. <b>{"0": 5, "1": 3, "2": 2, "1": 1, "is_in": 0}</b><br/>
            "is_in" key means that the user made a pick which is present somewhere in the result set but outside of the defined deviation classes
        """,
        default=points_default,
    )

    def is_complete(self):
        master = self.compsubmit_set.filter(
            user__username="master", user__is_staff=True
        ).first()
        if master and master.compchoice_set.count() == self.val_amount:
            return True
        return False

    def recalc(self):
        master = (
            get_user_model().objects.filter(username="master", is_staff=True).first()
        )
        for pos in (
            master.compsubmit_set.filter(competition=self).first().compchoice_set.all()
        ):
            for cs in self.compsubmit_set.filter(~models.Q(user=master)):
                correct = cs.compchoice_set.filter(val=pos.val).first()
                if correct:
                    diff = abs(correct.index - pos.index)
                    correct.diff = diff
                    correct.points = self.points.get(str(diff), 0)
                    if correct.points == 0:
                        is_in = self.points.get("is_in")
                        if is_in:
                            correct.points = is_in
                    if diff == 0:
                        correct.correct = True
                    correct.save()

    def get_submissions(self):
        master = (
            get_user_model().objects.filter(username="master", is_staff=True).first()
        )
        return self.compsubmit_set.filter(~models.Q(user=master))

    def get_pick_count(self):
        return (
            self.get_submissions()
            .values("compchoice__val")
            .annotate(player=F("compchoice__val"), count=Count("compchoice"))
            .filter(player__isnull=False)
            .order_by("-count")
            .values("player", "count")
        )

    def get_pick_distribution(self, order_by=["player"]):
        return (
            self.get_submissions()
            .values("compchoice__val", "compchoice__index")
            .annotate(
                index=F("compchoice__index"),
                player=F("compchoice__val"),
                count=Count("compchoice"),
            )
            .filter(player__isnull=False)
            .order_by(*order_by)
            .values("player", "index", "count")
        )


class CompSubmit(models.Model):
    user = models.ForeignKey(
        get_user_model(), null=True, blank=True, on_delete=models.CASCADE
    )
    competition = models.ForeignKey(
        Competition, null=True, blank=True, on_delete=models.CASCADE
    )
    submitted_at = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    place = models.IntegerField(default=0)
    disqualified = models.BooleanField(default=False)

    def __str__(self):
        return "{} - {}".format(self.competition, self.user)


class CompChoice(models.Model):
    index = models.IntegerField(default=0)
    val = models.CharField(max_length=255)
    csubmit = models.ForeignKey(
        CompSubmit, null=True, blank=True, on_delete=models.CASCADE
    )
    correct = models.BooleanField(default=False)
    diff = models.IntegerField(default=-1)
    points = models.IntegerField(default=0)

    def __str__(self):
        return "{} - {} ({})".format(
            self.csubmit.competition, self.csubmit.user, self.index
        )

    def save(self, *args, **kwargs):
        super(CompChoice, self).save(*args, **kwargs)
        master = (
            get_user_model().objects.filter(username="master", is_staff=True).first()
        )
        if self.csubmit.user == master:
            self.csubmit.competition.recalc()
