#!/bin/bash
ls -al
python manage.py makemigrations --settings=mouz.settings_prod
python manage.py migrate --settings=mouz.settings_prod
python manage.py collectstatic --no-input --settings=mouz.settings_prod
python manage.py run_huey --settings=mouz.settings_prod >> /var/log/mouz/huey.log 2>&1 &
python manage.py ropzpeek --settings=mouz.settings_prod >> /var/log/mouz/ropzpeek.log 2>&1 &
gunicorn --bind :12000 --workers 3 mouz.wsgi --capture-output >> /var/log/mouz/gunicorn.log 2>&1
